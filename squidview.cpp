/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

 squidview 0.8x

 A program to nicely browse your squid log
  
 (c) 2001 - 2017 Graeme Sheppard

 This program is not how it would be if I had to rewrite it from scratch.
 It started off very simple (quick and nasty) and needed to be fast because
 the computer was only a 486. So it was done in C. Then it would encounter
 a long request line, buffer overflow and seg fault. So I doubled the
 buffer size. OK for a while then same thing. Should've been C++ from the
 start because I kept doubling.

 A report generator a mile long, and a mile deep. Global variables like crazy.
 A half-baked keyboard input system. A half-baked console out display. The
 last one has a view good things about it.
 
 The program should have been split into several files at least. The code
 logic is followable which is something.
 
 Warning: those who complain about the source code might be requested to
 be the one who rewrites it :)
  
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#else
#  define SHAREDIR "/usr/local/share/squidview"
#  define PACKAGE "squidview"
#  define VERSION "0.7"
#endif

#include <string>
#include <sstream>
#include <vector>
#include <algorithm>

#include <curses.h>
#include <signal.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <ctype.h>
#include <string.h>

#include "squidview.h"

using namespace std;


ct con; // screen output
cUsersMode aTally; // an instance of users' tally

void ShowHighLightMessage (const string& sText);


void print (const string& sText)
{
  printf ("%s\n", sText.c_str());
}


// is the text null?
// this function fixed by Mike Reid
bool NullText (const char* szText)
{
  if (szText != 0)
  {
    const int length = strlen(szText);
    for (int index = 0; index < length; ++index)
    {
      if (!isspace(szText[index]))
        return false;
    }
  }

  // If got here then string is null.
  return true;
}


// string to lower case
// this function fixed by Mike Reid
void StrLwr (string& sString)
{
  const int length = sString.length();
  for (int index = 0; index < length; ++index)
    sString[index] = tolower(sString[index]);
}


// support the next function
inline bool CompactThis (string& sString, const string& sSubString)
{
  int iLen = sString.length();
  int iSubLen = sSubString.length();
  string sTemp;

  if (iSubLen > iLen)
    return false;
  sTemp = sString.substr (iLen - iSubLen, iSubLen);
  if (sSubString == sTemp)
  {
    sString.replace (iLen - iSubLen, iSubLen, "");
    return true;
  }
  else
    return false;
}


// Remove trailing spaces: " "s or "%20"s
void Compact (string& sString)
{
  bool bDone;

  for (;;)
  {
    bDone = true;
    if (CompactThis (sString, " "))
      bDone = false;
    if (CompactThis (sString, "%20"))
      bDone = false;
    if (bDone)
      return;
  }
}


// remove leading " "s
void NoLeadingSpaces (string& sText)
{
  while ((sText != "") && (sText [0] == ' '))
    sText.replace (0, 1, "");
}


// int to string conversion
string ItoS (tByteTotal iNumber)
{
  stringstream ssTemp;

  ssTemp << iNumber;
  return (ssTemp.str());
}


// 12345 to "12,345"
string ItoCommas (tByteTotal iNumber)
{
  string sReturn;
  int iIndex, iCount;

  sReturn = ItoS (iNumber);
  
  iIndex = sReturn.length();
  if (iIndex == 0)
    return sReturn;
  iIndex--;
  iCount = 4;
  while (iIndex >= 0)
  {
    if (--iCount == 0)
    {
      sReturn.insert (iIndex + 1, ",");
      iCount = 3;
    }
    iIndex--;
  }
  return sReturn;
}


// open a file, with large file support >4G
int OpenLargeFile (const char* szFilename)
{
#ifdef LARGE_FILE
  return open (szFilename, O_RDONLY | O_LARGEFILE);
#else
  return open (szFilename, O_RDONLY);
#endif
}


// return the size of a file
tFilePos GetFileSize (const string& sFileName)
{
  int iFD;
  tFilePos iPos;
  
  iFD = OpenLargeFile (sFileName.c_str());
  if (iFD < 0)
  {
    return 0;
  }
  iPos = lseek (iFD, 0, SEEK_END);
  close (iFD);
  return iPos;
}


// clears the screen
void MyCls()
{
  attroff (A_REVERSE);
  clear();
  refresh();
  move (0, 0);
}


bool RunProgramQuietly (const string& sCommand)
{
  if (system ((sCommand + " 2>/dev/null").c_str()) == 0)
    return true;
  sStatusMessage = string ("Error running command: " + sCommand);
  return false;
}


// execute a shell command
void RunProgramInConsole (const string& sCommand)
{
  MyCls();
  curs_set (1);
  echo();
  nocbreak();
  endwin();   // suggested by endwin(3)
  if (system (sCommand.c_str()))
    sStatusMessage = string ("Error running command: " + sCommand);
  keypad (stdscr, TRUE);
  curs_set (0);
  noecho();
  cbreak();
  MyCls();
}


// allows check of keyboard buffer to see if keypress ready
void GoForNoDelay()
{
  if (wTemp)
    delwin (wTemp);
  wTemp = newwin (1, 1, LINES - 1, COLS - 1);
  wrefresh (wTemp);
  nodelay (wTemp, true);
}


// return xxx.yy% of first number to second number
void CalcPercentage (tFilePos iCurrent, tFilePos iEnd,
                     string& sOutput)
{
  float fPC;
  int iNum;
  char szTemp [20] = "";

  if ((iCurrent == 0) || (iEnd == 0))
    sOutput = "  0.00%";
  else
  if (iCurrent >= iEnd)
    sOutput = "100.00%";
  else
  {
    iNum = int (10000.0 * iCurrent / iEnd);
    fPC = float (iNum) / 100.0;
    if (snprintf (szTemp, sizeof (szTemp), "%6.2f%%", fPC) < 0)
      sOutput = "  -.--%";
    else
      sOutput = szTemp;
  }
}


// parse "50000 250000 1000000" for size catches
// given a pointer to it, return the number and the new pointer
int NextCatchSize (const char** ppcText)
{
  const char* pcLetter;
  char cLetter;
  int iNum;
  
  iNum = 0;
  pcLetter = *ppcText;
  for (;;)
  {
    cLetter = *pcLetter;
    if (cLetter == '\0')
    {
      *ppcText = pcLetter;
      return iNum;
    }
    pcLetter++;
    if (cLetter == ' ')
    {
      *ppcText = pcLetter;
      return iNum;
    }
    if ((cLetter < '0') || (cLetter > '9'))
      return -1;
    iNum = iNum * 10 + cLetter - '0';
  }
}


string RightJustify (const string& sText, int iCols)
{
  string sTemp;

  if (int (sText.length()) >= iCols)
    return sText;
  sTemp.assign (iCols - int (sText.length()), ' ');
  sTemp += sText;
  return sTemp;
}


ct& ct::operator << (const char* szText)
{
  addstr (const_cast<char*> (szText));
  return *this;
}


ct& ct::operator << (const string& sText)
{
  addstr (const_cast<char*> (sText.c_str()));
  return *this;
}


ct& ct::operator << (char cLetter)
{
  addch (cLetter);
  return *this;
}


ct& ct::operator << (bool bYes)
{
  if (bYes)
    addstr ("yes");
  else
    addstr ("no");
  return *this;
}


ct& ct::operator << (int iNumber)
{
  string sTemp;

  sTemp = ItoS (iNumber);
  addstr (const_cast<char*> (sTemp.c_str()));
  return *this;
}


bool CheckInterrupt()
{
  for (;;)
  {
    switch (wgetch (wTemp))
    {
      case 's':
        return true;
      case ERR:
        return false;
    }
  }
}


bool CheckTicker()
{
  static time_t iTicker = 0;
  time_t iCurrentime_t;

  iCurrentime_t = time (0);
  if (iCurrentime_t == iTicker)
    return false;
  
  iTicker = iCurrentime_t;
  return true;
}


void UpdateTicker (const char* szText, tFilePos iPos, tFilePos iMax)
{
  string sTemp;

  CalcPercentage (iPos, iMax, sTemp);
  sTemp += string (" ") + szText + " | <s> to stop";
  ShowHighLightMessage (sTemp);
}


// check if need to open new file, return false on error
inline bool ReadingFile()
{
  if ((fReadingFile < 0) || (pszReadingFile != pszCurrentLog))
  {
    if (fReadingFile > 0)
      close (fReadingFile);
    fReadingFile = OpenLargeFile (pszCurrentLog);
    if (fReadingFile > 0)
    {
      pszReadingFile = pszCurrentLog;
      return true;
    }
    else
    {
      pszReadingFile = 0;
      return false;
    }
  }
  return true;
}


// goto log at a certain position, read a line into buffer
// return start of next line
// incorporates modified patches by Vaclav Haisman: 2x speed-up
tFilePos GetLine (tFilePos iOffset)
{
  tFilePos iBlock, iAvailable, iReturn;
  int iCount;
  char *pcBuffer;

  pcReqBuff = pcEmpty;
  
  iBlock = iLogFileSize - iOffset;
  if (iBlock <= 0)
    return 0;
  if (iBlock > nReadBuffer)
    iBlock = nReadBuffer;

  if (!ReadingFile())
    return 0;

  iReturn = 0;

  if (lseek (fReadingFile, iOffset, SEEK_SET) >= 0)
  {
    iAvailable = read (fReadingFile, &cDiskBuffer, iBlock);
    if (iAvailable > 0)
    {
      pcBuffer = cDiskBuffer;
      for (iCount = 0; iCount < iAvailable; iCount++)
      {
        if (*pcBuffer == cEOL)
          break;
        pcBuffer++;
      }
      if (iCount >= iAvailable)
        iCount = iAvailable - 1;
      iReturn = iOffset + iCount + 1;
      cDiskBuffer [iCount] = '\0';
      pcReqBuff = cDiskBuffer;
    }
  }

  return iReturn;
}


// return the line before, eg for going backwards
// incorporates modified patches by Vaclav Haisman: 2x speed-up
tFilePos GetPrevLine (tFilePos iOffset)
{
  tFilePos iBlock, iBase, iAvailable, iReturn;
  int iIndex;
  char* pcBuffer;

  pcReqBuff = pcEmpty;

  if (iOffset <= 0)
    return 0;

  iBlock = iOffset;
  if (iBlock > nReadBuffer)
    iBlock = nReadBuffer;

  if (!ReadingFile())
    return iOffset;

  iReturn = iOffset;

  iBase = iOffset - iBlock;
  if (lseek (fReadingFile, iBase, SEEK_SET) >= 0)
  {
    iAvailable = read (fReadingFile, &cDiskBuffer, iBlock);
    if (iAvailable > 3)
    {
      if (cDiskBuffer [iAvailable - 2] == cEOL)
      {
        iReturn = iBase + iAvailable - 2;
      }
      else
      {
        iIndex = iAvailable - 3;
        pcBuffer = cDiskBuffer + iIndex;
        while (iIndex > 0)
        {
          if (*pcBuffer == cEOL)
          {
            iReturn = iBase + iIndex + 1;
            pcReqBuff = cDiskBuffer + iIndex + 1;
            cDiskBuffer [iAvailable] = '\0';
            break;
          }
          iIndex--;
          pcBuffer--;
        }
        if (iIndex == 0)
          iReturn = iBase;
      }
    }
  }

  return iReturn;
}


// rationalise log file statistics
void CalcLastPage()
{
  tFilePos iThis, iNext;
  int iCount;

  iLogFileSize = GetFileSize (pszCurrentLog);
  if (iLogFileSize == 0)
  {
    iMaxLinesDown = 0;
    iLastPage = 0;
    iLastLinePos = 0;
    iLinesDown = 0;
    return;
  }

  iLastLinePos = GetPrevLine (iLogFileSize - 1);

  iThis = GetLine (0);
  for (iCount = 0; iCount < iMainLines; iCount++)
  {
    iNext = GetLine (iThis);
    if ((iNext == iThis) || (iNext == 0))
    {
      iMaxLinesDown = iCount;
      iLastPage = 0;
      if (iLinesDown > iMaxLinesDown)
        iLinesDown = iMaxLinesDown;
      return;
    }
    iThis = iNext;
  }
  iMaxLinesDown = iMainLines - 1;
  if (iLinesDown > iMaxLinesDown)
    iLinesDown = iMaxLinesDown;

  iThis = iLogFileSize;
  for (iCount = 0; iCount < iMainLines; iCount++)
    iThis = GetPrevLine (iThis);
  iLastPage = iThis;
}


// goto some point in log, place in middle of screen and
// highlight it
void CentreScreen (tFilePos iGoto)
{
  tFilePos iTempPos;
  int iCount, iHalf;

  if (iGoto >= iLastPage)
  {
    iLinesDown = 0;
    iPagePos = iTempPos = iLastPage;
    for (iCount = 0; iCount < iMaxLinesDown; iCount++)
    {
      if (iTempPos == iGoto)
        return;
      iTempPos = GetLine (iTempPos);
      iLinesDown++;
    }
    iLinesDown = iMaxLinesDown;
    return;
  }

  iHalf = iMaxLinesDown / 2;
  iLinesDown = 0;
  iTempPos = iGoto;
  for (iCount = 0; iCount < iHalf; iCount++)
  {
    if (iTempPos == 0)
    {
      iPagePos = 0;
      return;
    }
    iTempPos = GetPrevLine (iTempPos);
    iLinesDown++;
  }
  iPagePos = iTempPos;
}


// return space seperated column number iNum
// first = 1
void GetColumn (const char* pcText0, int iNum, string& sResult)
{
  int iColumn, iIndex, iStart;
  bool bCapture, bSkipSpace;
  char cLetter;
  const char* pcText;
  
  bCapture = iNum == 1 ? true : false;
  bSkipSpace = false;
  iStart = iIndex = 0;
  iColumn = 1;
  pcText = pcText0;

  for (;;)
  {
    cLetter = *pcText;
    if ((cLetter == 0) || (cLetter == cEOL) || (cLetter == cCR))
      break;
    if (cLetter == ' ')
    {
      if (bCapture)
        break;
      bSkipSpace = true;
    }
    else
    if (bSkipSpace)
    {
      if (++iColumn == iNum)
      {
        bCapture = true;
        iStart = iIndex;
      }
      bSkipSpace = false;
    }
    pcText++;
    iIndex++;
  }
  sResult = "";
  if (bCapture)
    sResult.append (pcText0 + iStart, iIndex - iStart);
}


// do we really need to see heaps of "http://" and "ftp://"?
void RemoveSlashes (string& sText)
{
  char szTemp [20];

  sText.copy (szTemp, 15);

  szTemp [7] = 0;
  if (strcmp (szTemp, "http://") == 0)
  {
    sText.replace (0, 7, "");
    return;
  }

  szTemp [6] = 0;
  if (strcmp (szTemp, "ftp://") == 0)
  {
    sText.replace (0, 6, "");
    return;
  }
}


void GetDomainName (const char* szRequest, string& sDomain)
{
  int iCount, iSize;
  
  GetColumn (szRequest, iTargetCol, sDomain);
  RemoveSlashes (sDomain);
  iSize = sDomain.length();
  for (iCount = 0; iCount < iSize; iCount++)
    if (sDomain [iCount] == '/')
    {
      sDomain.replace (iCount, iSize - iCount, "");
      return;
    }
}


// pad a string with spaces to the edge specified, eg screen
void Columnize (string& sText, int iWidth)
{
  int iLength, iSpaces;
  string sTemp;

  iLength = sText.length();
  if (iLength == iWidth)
    return;
  if (iLength > iWidth)
    sText.replace (iWidth, iLength - iWidth, "");
  else
  {
    iSpaces = iWidth - iLength;
    sTemp.assign (iSpaces, ' ');
    sText += sTemp;
  }
}


time_t GetTimeNumber (const char* szRequest)
{
  string sTemp;
  int iPos, iLength;
  char cLetter;
  time_t iTime;

  GetColumn (szRequest, iTimeCol, sTemp);
  if (sTemp == "")
    return 0;
    
  iTime = 0;
  iPos = 0;
  iLength = sTemp.length();
  for (;;)
  {
    if (iPos >= iLength)
      return iTime;
    cLetter = sTemp [iPos];
    if ((cLetter == '.') || (cLetter == 0))
        return iTime;
    if ((cLetter >= '0') && (cLetter <= '9'))
      iTime = (iTime * 10) + cLetter - '0';
    else
      return 0;
    iPos++;
  }
}


void CalcTime (time_t iTime, string& sDate)
{
  int iCount; 
  char szTemp [80]; 

  strncpy (szTemp, ctime (&iTime), sizeof (szTemp) - 1); 
// Ensure the time string is null-terminated. 
  szTemp [sizeof (szTemp) - 1] = '\0';
  iCount = strlen (szTemp); 
  if (iCount > 0) 
    if (szTemp [iCount - 1] == '\n') 
      szTemp [iCount - 1] = 0; 
  sDate = szTemp; 
}


void EraseStatusLine()
{
  move (iMainLines, 0);
  attroff (A_REVERSE);
  clrtoeol();
}


void ShowHighLightMessage (const string& sText)
{
  EraseStatusLine();
  move (iMainLines, 0);
  attron (A_REVERSE);
  con << sText;
  refresh();
}


void PromptForText (const string& sQuestion, string& sResult)
{
  char szTemp [nInputLen];

  EraseStatusLine();
  move (iMainLines, 0);
  attroff (A_REVERSE);
  con << sQuestion;
  curs_set (1);
  echo();
  getstr (szTemp);
  curs_set (0);
  noecho();
  EraseStatusLine();
  sResult = szTemp;
}


char PromptForKey (const string& sQuestion)
{
  char cKey;

  EraseStatusLine();
  move (iMainLines, 0);
  attroff (A_REVERSE);
  con << sQuestion;
  cKey = getch();
  EraseStatusLine();
  return cKey;
}


bool CharGood (char cLetter)
{
  if ((cLetter >= '0') && (cLetter <= '9'))
    return true;
  if ((cLetter >= 'A') && (cLetter <= 'Z'))
    return true;
  if ((cLetter >= 'a') && (cLetter <= 'z'))
    return true;
  switch (cLetter)
  {
    case '-':
    case '_':
      return true;
  }
  return false;
}


string CheckFileName (const string& sName)
{
  int iCount;
  char cLetter;
  string sReturn;

  for (iCount = 0; iCount < int (sName.length()); iCount++)
  {
    cLetter = sName [iCount];
    if (CharGood (cLetter) == false)
    {
      if ((cLetter < 32) || (cLetter > 126))
        return "Illegal character.";
      sReturn = string ("Illegal character: '") + cLetter + "'.";
      return sReturn;
    }
  }
  return "";
}


string CheckEmail (const string& sName)
{
  int iCount;
  char cLetter;
  string sReturn;

  for (iCount = 0; iCount < int (sName.length()); iCount++)
  {
    cLetter = sName [iCount];
    if ((cLetter != '@') && (cLetter != '.') && (CharGood (cLetter) == false))
    {
      if ((cLetter < 32) || (cLetter > 126))
        return "Illegal character.";
      sReturn = string ("Illegal character: '") + cLetter + "'.";
      return sReturn;
    }
  }
  return "";
}


void GetText (string& sText)
{
  char szTemp [nInputLen];

  curs_set (1);
  echo();
  getstr (szTemp);
  curs_set (0);
  noecho();
  sText = szTemp;
}


void GetFileName (string& sResult, bool* pbGood = 0)
{
  string sCheck;
  
  if (pbGood)
    *pbGood = true;
  GetText (sResult);
  if (sResult == "")
    return;

  sCheck = CheckFileName (sResult);
  if (sCheck == "")
  {
    sResult += sReportExt;
    return;
  }

  con << "Problem: " << sCheck << " Press any key.";
  getch();
  con << "\n";
  sResult = "";
  if (pbGood)
    *pbGood = false;
}


/* unused
void RemoveTrailingSpaces (string& sText)
{
  int iIndex, iKeep;
  bool bStrip;

  iIndex = iKeep = sText.length();
  if (iIndex == 0)
    return;
  iIndex--;
  bStrip = false;
  for (;;)
  {
    if (sText [iIndex] != ' ')
      break;
    if (--iIndex < 0)
    {
      sText = "";
      return;
    }
    bStrip = true;
    iKeep--;
  }
  if (bStrip)
    sText.resize (iKeep);
}
*/


// load the search phrases
int LoadWords()
{
  int iFile, iRead, iLetter, iLength;

  sWords = "";
  iLength = 0;
  iFile = open (sWordFile.c_str(), O_RDONLY);
  if (iFile <= 0)
    return 0;

  iRead = 1;
  while (iRead == 1)
  {
    iLetter = 0;
    iRead = read (iFile, &iLetter, 1);
    if (iRead == 1)
    {
      if (iLetter == cEOL)
      {
        if (iLength > 0)
          sWords += cSeperator;
        iLength = 0;
      }
      else
      if (iLetter >= ' ')
      {
        sWords += iLetter;
        iLength++;
      }
    }
  }
  close (iFile);

  if ((sWords != "") && (sWords [sWords.length() - 1] != cSeperator))
    sWords += cSeperator;
  return 1;
}


// save the search phrases
int SaveWords()
{
  int iIndex, iWrite, iReturn;
  char cLetter, cWrite;
  FILE* fOutput;

  fOutput = fopen (sWordFile.c_str(), "w");
  if (fOutput == NULL)
    return 0;

  iIndex = 0;
  iReturn = -1;
  while (iReturn == -1)
  {
    cLetter = sWords [iIndex++];
    cWrite = 0;
    switch (cLetter)
    {
      case 0:
        iReturn = 1;
        break;

      case cSeperator:
        cWrite = '\n';
        break;

      default:
        cWrite = cLetter;
    }
    if (cWrite)
    {
      iWrite = fwrite (&cWrite, 1, 1, fOutput);
      if (iWrite != 1)
        iReturn = 0;
    }
  }
  fclose (fOutput);
  return iReturn;
}


tIPnum GetIP (const string& sText)
{
  tIPnum iReturn = 0;
  int iLength = sText.length();
  int iIndex = 0;
  int iByte = 0;
  char cLetter = 0, cLast = 0;
  
  while (iIndex < iLength)
  {
    cLetter = sText [iIndex++];
    if (cLetter == '.')
    {
      if (cLast == '.')
        return 0;
      iReturn = (iReturn << 8) + iByte;
      iByte = 0;
    }
    else
    if ((cLetter >= '0') && (cLetter <= '9'))
    {
      iByte = (iByte * 10) + (cLetter - '0');
      if (iByte > 255)
        return 0;
    }
    else
      return 0;
    cLast = cLetter;
  }
  
  if (cLetter == '.')
    return 0;
    
  iReturn = (iReturn << 8) + iByte;
  return iReturn;  
}


// load aliases
void LoadAliases()
{
  int iFile, iRead, iLetter, iNames, iLine, iCount;
  string sLine, sColumn;
  tIPnum iIP;
  rAliasRecord rEntry;

  while (!vAliasList.empty())
    vAliasList.pop_back();
  while (!vAliasUserName.empty())
    vAliasUserName.pop_back();

  iNames = 0;
  iLine = 1;
  
  iFile = open (sAliasesFile.c_str(), O_RDONLY);
  if (iFile <= 0)
    return;

  iRead = 1;
  sLine = "";
  while (iRead == 1)
  {
    iLetter = 0;
    iRead = read (iFile, &iLetter, 1);
    if (iRead == 1)
    {
      if (iLetter == cEOL)
      {
        GetColumn (sLine.c_str(), 1, sColumn);
        if (sColumn != "")
        {
          vAliasUserName.push_back (sColumn);
          iCount = 2;
          for (;;)
          {
            GetColumn (sLine.c_str(), iCount++, sColumn);
            if (sColumn == "")
              break;
            iIP = GetIP (sColumn);
            if (iIP <= 0)
            {
              sStatusMessage = string ("Error in aliases file at line ") +
                ItoS (iLine);
              close (iFile);
              return;
            }
            rEntry.iIPnum = iIP;
            rEntry.iUserNum = iNames;
            vAliasList.push_back (rEntry);
          }
          iNames++;
        }
        sLine = "";
        iLine++;
      }
      else
        sLine += char (iLetter);
    }
  }
  close (iFile);
}


// does szText contain any of the search phrases?
// skip lines if they match with a ! (skip) search word
eSearch SearchString (const char* szText)
{
  const char *pcWords, *pcWords2;
  const char *pcText0, *pcText, *pcText2;
  char cLetter, cFirst;
  bool bFound;
  eSearch iReturn;

  pcWords = sWords.c_str();
  pcText0 = szText;
  while (*pcWords)
  {
    iReturn = hit;
    if (*pcWords == '!')
    {
      iReturn = veto;
      pcWords++;
    }
    cFirst = *pcWords;
    pcText = pcText0;
    while ((cLetter = *pcText) != 0)
    {
      if (cLetter == cFirst)
      {
        bFound = true;
        pcWords2 = pcWords + 1;
        pcText2 = pcText + 1;
        while (*pcWords2 != cSeperator)
        {
          if (*pcText2 != *pcWords2)
          {
            bFound = false;
            break;
          }
          pcWords2++;
          pcText2++;
        }
        if (bFound)
          return iReturn;
      }
      pcText++;
    }
    while (*pcWords != cSeperator)
      pcWords++;
    pcWords++;
  }
  return miss;
}


// find full name of user
string GetLongUserName (const string& sLoginName)
{
  string sUserLine, sUserName, sTemp, sReturn;
  int iFile, iRead;
  char cLetter;

  iFile = open (sUsersFile.c_str(), O_RDONLY);
  if (iFile <= 0)
    return "";

  sUserName = sLoginName;
  StrLwr (sUserName);
  sUserLine = sReturn = "";

  for (;;)
  {
    iRead = read (iFile, &cLetter, 1);
    if (iRead < 1)
      break;
    if (cLetter == cEOL)
    {
      GetColumn (sUserLine.c_str(), 1, sTemp);
      if (sTemp == sUserName)
      {
        sReturn = sUserLine;
        sReturn.replace (0, sUserName.length() + 1, "");
        break;
      }
      sUserLine = "";
    }
    else
    if (cLetter != cCR)
      sUserLine += cLetter;
  }
  close (iFile);
  return sReturn;
}


string LookupIP (const string& sIPtext)
{
  tIPnum iIP;
  int iCount, iTotal;
  
  iIP = GetIP (sIPtext);
  if (iIP <= 0)
    return "";
  iTotal = vAliasList.size();
  
  for (iCount = 0; iCount < iTotal; iCount++)
    if (vAliasList [iCount].iIPnum == iIP)
      return vAliasUserName [vAliasList [iCount].iUserNum];
  
  return "";
}


string ChopOffNetwork (const string& sSourceIP, int iLength)
{
  string sTemp = sSourceIP;
  int iLen, iIndex;
  
  for (;;)
  {
    if ((iLen = sTemp.length()) <= iLength)
      return sTemp;
    iIndex = 0;
    for (;;)
    {
      if (iIndex >= iLen)
        return sTemp;
      if (sTemp [iIndex] == '.')
      {
        sTemp.replace (0, iIndex + 1, "");
        break;
      }
      iIndex++;
    }
  }
}


// get username from column in request line, lower case it, remove "%20"s
// if dashed shorten the returned user by default
string GetUserName (const char* szRequest, bool bTruncate)
{
  string sTemp, sIP;
  
  GetColumn (szRequest, iUserCol, sTemp);

  if (bAliases  && (sTemp == "-"))
  {
    GetColumn (szRequest, iIPCol, sIP);
    sIP = LookupIP (sIP);
    if (sIP != "")
      return sIP;
  }
  
  if (bLookupIP && (sTemp == "-"))
  {
    GetColumn (szRequest, iIPCol, sTemp);
    if (!bTruncate)
      return sTemp;
    else
      return ChopOffNetwork (sTemp, iUserNameLen);
  }

  Compact (sTemp);
  StrLwr (sTemp);
  return sTemp;
}


// return the number of bytes in the request
int GetRequestSize (const char* szRequest)
{
  string sTemp;
  tFilePos iSize;

  GetColumn (szRequest, iSizeCol, sTemp);
  if (sTemp == "")
    return 0;
  iSize = atoi (sTemp.c_str());
  return iSize;
}


void GetRequestCacheHit (const char* szRequest, char& cHit, int& iHitNumber)
{
  string sBoth, sLeft, sRight;
  int iCount, iLength, iSlash = -1;
  
  cHit = ' ';
  iHitNumber = 0; 
  GetColumn (szRequest, iCacheHitCol, sBoth);
  iLength = sBoth.length();
  for (iCount = 0; iCount < iLength; iCount++)
    if (sBoth [iCount] == '/')
    {
      iSlash = iCount;
      break;
    }
  if (iSlash == -1)
    return;
  
  sLeft = sRight = "";
  if (iSlash >= 1)
    sLeft = sBoth.substr (0, iSlash);
  if (iSlash < iLength - 1)
    sRight = sBoth.substr (iSlash + 1, iLength - iSlash - 1);
  
  // should be in table form
  // the cache hits, the cache misses and other

  if (sLeft == "TCP_HIT")                 cHit = 'H'; else
  if (sLeft == "TCP_IMS_HIT")             cHit = 'I'; else
  if (sLeft == "TCP_REFRESH_HIT")         cHit = 'R'; else
  if (sLeft == "TCP_REFRESH_UNMODIFIED")  cHit = 'R'; else
  if (sLeft == "TCP_NEGATIVE_HIT")        cHit = 'N'; else
  if (sLeft == "TCP_MEM_HIT")             cHit = 'M'; else
  if (sLeft == "TCP_HIT_ABORTED")         cHit = 'A'; else
  if (sLeft == "TCP_REFRESH_FAIL_OLD")    cHit = 'F'; else
  if (sLeft == "TCP_INM_HIT")             cHit = 'U'; else

  if (sLeft == "TCP_MISS")                cHit = ' '; else
  if (sLeft == "TCP_REFRESH_MISS")        cHit = 'm'; else
  if (sLeft == "TCP_REFRESH_MODIFIED")    cHit = 'm'; else
  if (sLeft == "TCP_CLIENT_REFRESH")      cHit = 'r'; else
  if (sLeft == "TCP_CLIENT_REFRESH_MISS") cHit = 'c'; else
  if (sLeft == "TCP_MISS_ABORTED")        cHit = 'a'; else
  if (sLeft == "TCP_SWAPFAIL_MISS")       cHit = 'f'; else
  if (sLeft == "TCP_TUNNEL")              cHit = 't'; else
  if (sLeft == "TCP_REDIRECT")            cHit = 'o'; else

  if (sLeft == "TCP_DENIED")              cHit = 'd'; else
  if (sLeft == "TAG_NONE")                cHit = '0'; else
  if (sLeft == "NONE")                    cHit = '0'; else
    cHit = '?';
  
  iHitNumber = atoi (sRight.c_str());
}


// return Request Flags
void GetRequestFlags (const char* szRequest, string& sFlags)
{
  tFilePos iSize, iTemp;
  int iCount;
  const char* pcLetter;
  char cLetter;
  string sTemp;
  
  switch (SearchString (szRequest))
  {
    case hit:
      sFlags = "w";
      break;

    case miss:
      sFlags = " ";
      break;

    case veto:
      sFlags = "-";
      break;
  }

  if (bAliases && (sFlags == " "))
  {
    sTemp = GetUserName (szRequest, false);
    switch (SearchString (sTemp.c_str()))
    {
      case hit:
        sFlags = "w";
        break;

      case veto:
        sFlags = "-";
        break;

      case miss:
        break;
    }
  }
  
  iSize = GetRequestSize (szRequest);
  pcLetter = sSizeHits.c_str();
  iCount = 0;
  for (;;)
  {
    iTemp = NextCatchSize (&pcLetter);
    if (iTemp <= 0)
      break;
    if (iTemp >= iSize)
      break;
    iCount++;
  }
  if (iCount == 0)
    sFlags += ' ';
  else
    sFlags += char (iCount + '0');
  
  GetRequestCacheHit (szRequest, cLetter, iCount);
  sFlags += cLetter;

  cLetter = ' ';
  if (iRepFocus == user)
  {
    sTemp = GetUserName (szRequest, false);
    if (sTemp == sRepFocus)
      cLetter = 'f';
  }
  sFlags += cLetter;
}


inline bool SearchHit (const string& sFlags)
{
  if ((iRepFocus == user) && (sFlags [3] != 'f'))
    return false;
  if (sFlags [0] == 'w')
    return true;
  if (sFlags [0] == '-')
    return false;
  if (iSizeHitGrade == 0)
    return false;
  if (sFlags [1] >= char (iSizeHitGrade + '0'))
    return true;
  return false;
}


void GetPaddedUserName (const char* szRequest, string& sUser)
{
  sUser = GetUserName (szRequest, true);
  Columnize (sUser, iUserNameLen);
}


// search the log file in up (-1) or down (+1) direction
void SearchWords (int iDir)
{
  tFilePos iCurrent, iTempPos;
  string sFlags, sDispLine;

  if ((iDir < 0) && (iPagePos == 0) && (iLinesDown == 0))
    return;
  if ((iDir > 0) && (iPagePos >= iLastPage) && (iLinesDown == iMaxLinesDown))
    return;

  iCurrent = iLinePos;
  if (iDir > 0)
    iCurrent = GetLine (iCurrent);

  for (;;)
  {
    if (iDir > 0)
      iTempPos = GetLine (iCurrent);
    else
      iTempPos = GetPrevLine (iCurrent);
      
    GetRequestFlags (pcReqBuff, sFlags);
    if (SearchHit (sFlags))
    {
      if (iDir > 0)
        CentreScreen (iCurrent);
      else
        CentreScreen (iTempPos);
      return;
    }

    if (iDir < 0)
    {
      iCurrent = iTempPos;
      if (iCurrent <= 0)
      {
        iPagePos = 0;
        iLinesDown = 0;
        return;
      }
    }
    else
    {
      if ((iTempPos == 0) || (iTempPos == iCurrent))
      {
        iPagePos = iLastPage;
        iLinesDown = iMaxLinesDown;
        return;
      }
      iCurrent = iTempPos;
    }

    if (CheckInterrupt())
      return;
    if (CheckTicker())
      UpdateTicker ("Searching...", iCurrent, iLastPage);
  }
}


// remove seconds from time string
inline void RemoveSeconds (string& sTime)
{
  if (sTime.length() > 21)
    sTime.replace (16, 3, "");
}


// get the request's time out of it
string RequestToTime (const char* szRequest)
{
  string sTime;
  time_t iTime;
  
  if (iLogFileSize == 0) 
    return "";
    
  if (NullText (szRequest))
    return "";
  
  iTime = GetTimeNumber (szRequest);
  CalcTime (iTime, sTime);
  RemoveSeconds (sTime);
  return sTime;
}


// get the time from an arbitary point in log file
string FilePosToTime (tFilePos iCurrent)
{
  if (iLogFileSize == 0)
    return "";

  GetLine (iCurrent);
  return RequestToTime (pcReqBuff);
}


bool ReportOptions()
{
  string sTemp, sTemp1, sTemp2, sWordMode, sBUTmode, sBDTmode, sBDImode;
  int iTemp, iKey;
  bool bGood;
  
  for (;;)
  {
    MyCls();
    
    if (iRepStart == 0)
      sTemp1 = "begining of log";
    else
      sTemp1 = FilePosToTime (iRepStart);

    if (iRepEnd == 0)
      sTemp2 = "end of log";
    else
      sTemp2 = FilePosToTime (iRepEnd);

    switch (iRepWordHits)
    {
      case no_word:
        sWordMode = "none";
        break;

      case text:
        sWordMode = "normal text";
        break;

      case CSV:
        sWordMode = "CSV";
        break;

      default:
        sWordMode = "<error>";
        break;
    }
    
    switch (iRepBUT)
    {
      case nBUT_none:
        sBUTmode = "none";
        break;

      case nBUT_notveto:
        sBUTmode = "domains not skipped";
        break;

      case nBUT_all:
        sBUTmode = "all domains";
        break;

      default:
        sBUTmode = "<error>";
        break;
    }

    switch (iRepBDT)
    {
      case nBDT_none:
        sBDTmode = "none";
        break;

      case nBDT_all:
        sBDTmode = "all users";
        break;

      case nBDT_user:
        sBDTmode = "only user " + sRepBDTuser;
        break;

      default:
        sBDTmode = "<error>";
        break;
    }

    switch (iRepBDI)
    {
      case nBDI_notveto:
        sBDImode = "domains not skipped";
        break;

      case nBDI_all:
        sBDImode = "all domains";
        break;

      default:
        sBDImode = "<error>";
        break;
    }

    con << "Log A Report Options\n"
        << "\na. start from " << sTemp1
        << "\nb. finish at " << sTemp2
        << "\nc. report file name: " << sRepFileName
        << "\nd. title of report: " << sRepTitle
        << "\ne. cache hit report: " << bRepCacheReport
        << "\nf. user bandwidth totals: " << sBUTmode
        << "\ng. domain bandwidth totals: " << sBDTmode
        << "\nh. domain bandwidth includes: " << sBDImode
        << "\ni. word hit action: " << sWordMode
        << "\nj. normal text: total columns " << iRepColumns 
        << "\nk. normal text: split long lines: " << bRepSplit
        << "\nl. normal text: show request size: " << bRepShowSize
        << "\nm. CSV: field seperator: \"" << sRepSeperator << "\""
        << "\nn. CSV: target columns " << iRepCSVcols
        << "\n\nPress <enter> to go or q to quit:";

    iKey = getch();
    con << "\n\n";
    switch (iKey)
    {
      case 'a':
        iRepStart = iRepStart == 0 ? iLinePos : 0;
        break;
        
      case 'b':
        iRepEnd = iRepEnd == 0 ? iLinePos : 0;
        break;

      case 'c':
        con << "New file name : ";
        GetFileName (sTemp, &bGood);
        if (bGood)
          sRepFileName = sTemp;
        break;

      case 'd':
        con << "New title : ";
        GetText (sRepTitle);
        break;

      case 'e':
        bRepCacheReport = !bRepCacheReport;
        break;

      case 'f':
        if (++iRepBUT >= 3)
          iRepBUT = 0;
        break;

      case 'g':
        MyCls();
        if (iRepFocus == user)
          sTemp1 = sRepFocus;
        else
          sTemp1 = "";
        sTemp2 = "";
        GetLine (iLinePos);
        if (NullText (pcReqBuff))
          sTemp2 = "";
        else
          sTemp2 = GetUserName (pcReqBuff, false);
        con << "Bandwidth By Domain Option:-\n\n"
               "1.  no domain totals\n"
               "2.  all users contribute to domain totals\n"
               "3.  only focus user " << sTemp1 << "\n"
               "4.  only current user " << sTemp2 << "\n"
               "5.  specify a different user\n\n"
               "Your choice: ";
        iKey = getch();
        con << "\n\n";
        sTemp = "NON3";
        switch (iKey)
        {
          case '1':
            iRepBDT = nBDT_none;
            break;

          case '2':
            iRepBDT = nBDT_all;
            break;

          case '3':
            sTemp = sTemp1;
            break;

          case '4':
            sTemp = sTemp2;
            break;

          case '5':
            con << "Enter user name: ";
            GetText (sTemp);
            con << "\n";
            StrLwr (sTemp);
            break;
        }
        if (sTemp != "NON3")
        {
          if (sTemp == "")
          {
            con << "No user there. Press any key. ";
            getch();
          }
          else
          {
            sRepBDTuser = sTemp;
            iRepBDT = nBDT_user;
          }
        }
        break;

      case 'h':
        if (++iRepBDI >= 2)
          iRepBDI = 0;
        break;

      case 'i':
        if (++iRepWordHits >= 3)
          iRepWordHits = 0;
        break;

      case 'j':
        con << "New normal columns: ";
        GetText (sTemp);
        iTemp = atoi (sTemp.c_str());
        if ((iTemp >= 45) && (iTemp <= 256))
          iRepColumns = iTemp;
        break;
        
      case 'k':
        bRepSplit = !bRepSplit;
        break;
        
      case 'l':
        bRepShowSize = !bRepShowSize;
        break;
        
      case 'm':
        con << "New CSV seperator: ";
        GetText (sRepSeperator);
        break;

      case 'n':
        con << "New CSV target max size: ";
        GetText (sTemp);
        iTemp = atoi (sTemp.c_str());
        if ((iTemp > 10) && (iTemp < 600))
          iRepCSVcols = iTemp;
        break;

      case cEOL:
      case cCR:
        return true;

      case 'q':
        return false;
    }
  }
}


void KeepExtension (string& sTarget, int iColumns)
{
  int iLen = sTarget.length();
  int iIndex, iRemove, iDomainStart, iPathStart;
  
  if ((iRemove = iLen - iColumns) < 0)
    return;
    
  if (!bRepKeepExt)
  {
    sTarget.replace (iColumns, iRemove, "");
    return;
  }
  
  iIndex = iLen - 1;
  for (;;)
  {
    if (iIndex < 0)
    {
      iDomainStart = 0;
      break;
    }
    if (sTarget [iIndex] == ' ')
    {
      iDomainStart = iIndex + 1;
      break;
    }
    iIndex--;
  }

  iIndex = iDomainStart;
  for (;;)
  {
    if (iIndex >= iLen)
    {
      iPathStart = iLen;
      break;
    }
    if (sTarget [iIndex] == '/')
    {
      iPathStart = iIndex + 1;
      break;
    }
    iIndex++;
  }

  if (iLen - iPathStart >= iRemove)
  {
    sTarget.replace (iPathStart, iRemove, "");
    return;
  }
  
  sTarget.replace (iColumns, iRemove, "");
}


inline sRecordPointer sRecordPointer::operator= (const sRecordPointer& sFrom)
{
  iBytes = sFrom.iBytes;
  iHits = sFrom.iHits;
  iRecordNumber = sFrom.iRecordNumber;
  return *this;
}


void sListRecords::Empty()
{
  while (!vList.empty())
    vList.pop_back();
}


bool CompareBytes (const sRecordPointer& rLeft, const sRecordPointer& rRight)
{
  if (rLeft.iBytes < rRight.iBytes)
    return false;
  else
    return true;
}


bool CompareHits (const sRecordPointer& rLeft, const sRecordPointer& rRight)
{
  if (rLeft.iHits > rRight.iHits)
    return false;
  else
    return true;
}


void sListRecords::Sort (eSortType nSelect)
{
  bool bDone, bSwitch;
  int iIndex;
  sRecordPointer rTemp;
  
  if (vList.size() < 2)
    return;
    
  if (!bRepMySort)
  {
    switch (nSelect)
    {
      case nSortBytes:
        iSortFlag = 1014;
        sort (vList.begin(), vList.end(), CompareBytes);
        iSortFlag = 0;
        return;

      case nSortHits:
        iSortFlag = 1015;
        sort (vList.begin(), vList.end(), CompareHits);
        iSortFlag = 0;
        return;
    }
  }
  
  iSortFlag = 824;
  
  for (;;)
  {
    bDone = true;
    for (iIndex = vList.size() - 2; iIndex >= 0; iIndex--)
    {
      bSwitch = false;
      switch (nSelect)
      {
        case nSortHits:
          if (vList [iIndex].iHits > vList [iIndex + 1].iHits)
            bSwitch = true;
          break;

        case nSortBytes:
          if (vList [iIndex].iBytes < vList [iIndex + 1].iBytes)
            bSwitch = true;
          break;
      }
      if (bSwitch)
      {
        bDone = false;
        rTemp = vList [iIndex];
        vList [iIndex] = vList [iIndex + 1];
        vList [iIndex + 1] = rTemp;
      }
    }
    if (bDone)
    {
      iSortFlag = 0;
      return;
    }
  }
}


class GenerateReport
{
  public:
    void Run();

    enum eDone {done, ioerror, keybreak};

  private:
    void WriteReportLine (const string& sText);
    void HandleCSV();
    void HandleNormal();
    void ZeroCounters();

    bool bLogOutput;
    bool bMadeOutput;
    FILE* fOutput;
    string sBytePad, sFlags;
    time_t iLastime_t;
    tByteTotal iRequestSize;
    string sStatusLine;
    bool bDisplayOutput;
    
    vector <sUserRecord> vUserDetails;
    vector <sDomainRecord> vDomainDetails;
    sListRecords rUserPointers;
    sListRecords rDomainPointers;
};


// to compile on Slackware and other oldies don't
// use clear() because of naming problems: ncurses
// has a macro "clear" and it takes over.
inline void GenerateReport::ZeroCounters()
{
  while (!vUserDetails.empty())
    vUserDetails.pop_back();
  while (!vDomainDetails.empty())
    vDomainDetails.pop_back();
  rUserPointers.Empty();
  rDomainPointers.Empty();
}


inline void GenerateReport::WriteReportLine (const string& sText)
{
  if (bLogOutput == false)
    return;

  if (bDisplayOutput)
  {
    EraseStatusLine();
    refresh();
    printf ("%s\r", sText.c_str());
    ShowHighLightMessage (sStatusLine); /// don't like this bit
  }
    
  if (fwrite (sText.c_str(), 1, sText.length(), fOutput) != sText.length())
    throw ioerror;
}


inline void GenerateReport::HandleCSV()
{
  static string sTemp;
  static string sDispLine;
  static time_t iTime;

  GetColumn (pcReqBuff, iSizeCol, sTemp);
  sDispLine = sTemp + sRepSeperator;
  sTemp = GetUserName (pcReqBuff, true);
  sDispLine += sTemp + sRepSeperator;
  sDispLine += sFlags + sRepSeperator;
  GetColumn (pcReqBuff, iTimeCol, sTemp);
  sDispLine += sTemp + sRepSeperator;
  iTime = GetTimeNumber (pcReqBuff);
  CalcTime (iTime, sTemp);
  RemoveSeconds (sTemp);
  sDispLine += sTemp + sRepSeperator;
  GetColumn (pcReqBuff, iTargetCol, sTemp);
  KeepExtension (sTemp, iRepCSVcols);
  sDispLine += sTemp + "\n";
  WriteReportLine (sDispLine);
}


inline void GenerateReport::HandleNormal()
{
  static time_t iTime;
  const time_t iTimeMod = 60;
  static string sTemp1;
  static string sTemp2;
  static string sTarget;
  static string sTime;
  int iColsNotCSV, iCount, iDLfrom, iDLsize;
  
  iTime = GetTimeNumber (pcReqBuff);
  iTime = iTime - (iTime % iTimeMod);
  if (iTime != iLastime_t)
  {
    iLastime_t = iTime;
    CalcTime (iTime, sTime);
    RemoveSeconds (sTime);
  }
  else
    sTime = "";

  if (bRepShowSize)
    sTemp1 = RightJustify (ItoCommas (iRequestSize), nSizeCols) + " ";
  else
    sTemp1 = "";

  GetPaddedUserName (pcReqBuff, sTemp2);
  sTemp1 += sTemp2 + " " + sFlags + " ";
  if (sBytePad == "")
    sBytePad.assign (sTemp1.length(), ' ');

  if (sTime != "")
    WriteReportLine (sBytePad + sTime + "\n");

  GetColumn (pcReqBuff, iTargetCol, sTarget);
  RemoveSlashes (sTarget);

  if (bRepSplit == false)
  {
     sTemp1 += sTarget;
     KeepExtension (sTemp1, iRepColumns);
     sTemp1 += "\n";
     WriteReportLine (sTemp1);
  }
  else
  {
    iColsNotCSV = iRepColumns - sBytePad.length();
    if (iRepColumns < 5)
      iRepColumns = 5;
    iDLfrom = 0;
    iCount = sTarget.length();
    iDLsize = iCount;
    if (iDLsize > iColsNotCSV)
      iDLsize = iColsNotCSV;
    for (;;)
    {
      WriteReportLine (sTemp1 + sTarget.substr (iDLfrom, iDLsize) + "\n");
      iDLfrom += iDLsize;
      if (iDLfrom >= iCount)
        break;
      iDLsize = iCount - iDLfrom;
      if (iDLsize > iColsNotCSV)
        iDLsize = iColsNotCSV;
      sTemp1 = sBytePad;
    }
  }
}


// make the report
void GenerateReport::Run()
{
  tFilePos iCurrent, iTempPos, iStopPos, iScanArea;
  eDone iResult;
  int iHits, iVectorCount, iNumRecords, iThisEntry, iPointer;
  tByteTotal iByteTotal, iSearchHitTotal, iFocusTotal, iDashTotal;
  tByteTotal iUserBandWidth, iDomainBandWidth;
  tByteTotal iCacheHitBytes;
  int iCacheHitRequests, iRequests;
  int iFile, iRead;
  string sDispLine, sTemp, sTemp2, sUserName;
  sRecordPointer rPointer;
  sUserRecord rUserRecord;
  sDomainRecord rDomainRecord;
  bool bHandled = false, bTemp;
  char cLetter;
  time_t iTime;
  int iLine;
    
  bLogOutput = sRepFileName == "" ? false : true;
  iCurrent = iRepStart;
  iStopPos = iRepEnd == 0 ? -1 : iRepEnd;
  iScanArea = (iRepEnd == 0 ? iLastPage : iRepEnd) - iRepStart;
  sBytePad = "";
  iByteTotal = iSearchHitTotal = iFocusTotal = iDashTotal = 0;
  iUserBandWidth = iDomainBandWidth = 0;
  iCacheHitBytes = iCacheHitRequests = iRequests = 0;
  iHits = 0;
  iLine = 1;
  iLastime_t = -1;
   
  if (bLogOutput)
  {
    fOutput = fopen (string (sPathToFiles + "/" + sRepFileName)
                     .c_str(), "w");
    if (fOutput == NULL)
    {
      sStatusMessage = "Could not write to " + sRepFileName;
      return;
    }
  }

  ZeroCounters();
  GoForNoDelay();
  bMadeOutput = false;
  iTime = time (0);

  sStatusLine = "";
  bDisplayOutput = true;
  
  try
  {
    if (sRepTitle != "")
      WriteReportLine (sRepTitle + "\n\n");

    sTemp = FilePosToTime (iCurrent);
    if (iRepEnd == 0)
      sTemp2 = FilePosToTime (iLastPage);
    else
      sTemp2 = FilePosToTime (iRepEnd);

    if (sTemp2 == sTemp)
      WriteReportLine ("For " + sTemp + "\n\n");
    else
      WriteReportLine ("For " + sTemp + " to " + sTemp2 + "\n\n");
    
    for (;;)
    {
      if ((iStopPos > 0) && (iCurrent >= iStopPos))
        throw done;
      iTempPos = GetLine (iCurrent);
      if ((iTempPos == 0) || (iTempPos == iCurrent))
        throw done;
      bTemp = false;
      if (!NullText (pcReqBuff))
      {
        GetRequestFlags (pcReqBuff, sFlags);
        if (sFlags.find ("d") == string::npos) // check for squid denied
          bTemp = true;
      }
      if (bTemp)
      {
        iRequestSize = GetRequestSize (pcReqBuff);
        iByteTotal += iRequestSize;
        iRequests++;
        GetColumn (pcReqBuff, iUserCol, sTemp);
        if (sTemp == "-")
          iDashTotal += iRequestSize;
          
        bHandled = SearchHit (sFlags);
        if (bHandled)
        {
          iHits++;
          iSearchHitTotal += iRequestSize;
          if (bLogOutput)
          {
            switch (iRepWordHits)
            {
              case text:
                HandleNormal();
                bMadeOutput = true;
                break;
                
              case CSV:
                HandleCSV();
                bMadeOutput = true;
                break;
            }
          }
        }

        bTemp = false;
        if (iRepBUT == nBUT_all)
          bTemp = true;
        if ((iRepBUT == nBUT_notveto) && (sFlags [0] != '-'))
          bTemp = true;
        if (iRequestSize <= 0)
          bTemp = false;

        if (bTemp)
        {
          iUserBandWidth += iRequestSize;
          sUserName = GetUserName (pcReqBuff, false);
          if (sUserName != "")
          {
            iNumRecords = rUserPointers.vList.size();
            iThisEntry = -1;
            for (iVectorCount = iNumRecords - 1;
                 iVectorCount >= 0;
                 iVectorCount--)
            {
              iPointer = rUserPointers.vList [iVectorCount].iRecordNumber;
              if (sUserName == vUserDetails [iPointer].sLoginName)
              {
                iThisEntry = iVectorCount;
                rUserPointers.vList [iVectorCount].iBytes += iRequestSize;
                rUserPointers.vList [iVectorCount].iHits++;
                break;
              }
            }
            if (iThisEntry == -1)
            {
              rPointer.iBytes = iRequestSize;
              rPointer.iHits = 1;
              rPointer.iRecordNumber = iNumRecords;
              rUserPointers.vList.push_back (rPointer);
              
              rUserRecord.sLoginName = sUserName;
              rUserRecord.sFullName = "";
              vUserDetails.push_back (rUserRecord);
            }
          }
        }

        bTemp = false;
        if (iRepBDT == nBDT_all)
          bTemp = true;
        if (iRepBDT >= nBDT_user)
        {
          sUserName = GetUserName (pcReqBuff, false);
          if (sUserName == sRepBDTuser)
            bTemp = true;
        }
        if ((iRepBDI == nBDI_notveto) && (sFlags [0] == '-'))
          bTemp = false;
        if (iRequestSize <= 0)
          bTemp = false;

        if (bTemp)
        {
          iDomainBandWidth += iRequestSize;
          GetDomainName (pcReqBuff, sTemp);
          iNumRecords = rDomainPointers.vList.size();
          iThisEntry = -1;
          for (iVectorCount = iNumRecords - 1;
               iVectorCount >= 0;
               iVectorCount--)
          {
            iPointer = rDomainPointers.vList [iVectorCount].iRecordNumber;
            if (sTemp == vDomainDetails [iPointer].sDomainName)
            {
              iThisEntry = iVectorCount;
              rDomainPointers.vList [iVectorCount].iBytes += iRequestSize;
              rDomainPointers.vList [iVectorCount].iHits++;
              break;
            }
          }
          if (iThisEntry == -1)
          {
            rPointer.iBytes = iRequestSize;
            rPointer.iHits = 1;
            rPointer.iRecordNumber = iNumRecords;
            rDomainPointers.vList.push_back (rPointer);
              
            rDomainRecord.sDomainName = sTemp;
            vDomainDetails.push_back (rDomainRecord);
          }
        }

        if ((sFlags [2] >= 'A') && (sFlags [2] <= 'Z'))
        {
          iCacheHitBytes += iRequestSize;
          iCacheHitRequests++;
        }
      }

      if ((iStopPos > 0) && (iTempPos >= iStopPos))
        throw done;
      iCurrent = iTempPos;

      if ((iRepFast) && (iLine % iRepFast == 0))
      {
        rUserPointers.Sort (sListRecords::nSortHits);
        rDomainPointers.Sort (sListRecords::nSortHits);
      }
      iLine++;

      if (CheckInterrupt())
        throw keybreak;

      if (bHandled || CheckTicker())
      {      
        sTemp = string ("complete: ") +
                ItoCommas (iByteTotal) + " bytes, " +
                ItoCommas (iHits) + " hits";
        UpdateTicker (sTemp.c_str(), iCurrent - iRepStart, iScanArea);
      }
    }
  }
  catch (eDone iTemp)
  {
    iResult = iTemp;
  }

  bDisplayOutput = false;
  MyCls();

  if (iResult == done)
  {
    try
    {
      WriteReportLine ("\n" +
        RightJustify (ItoCommas (iByteTotal), nSizeCols) +
        " scanned bytes, " + ItoCommas (iHits) + " hits total " + 
        ItoCommas (iSearchHitTotal) + " bytes\n");

      WriteReportLine ("\nBytes attributed to \"-\" user: " +
        ItoCommas (iDashTotal) + "\n");

      if (bRepCacheReport)
      {
        bMadeOutput = true;
        WriteReportLine ("\nCache hit statistics:\n");
        CalcPercentage (iCacheHitRequests, iRequests, sTemp);
        WriteReportLine (ItoCommas (iCacheHitRequests) + " requests out of "
          + ItoCommas (iRequests) + ": " + sTemp + "\n");
        CalcPercentage (iCacheHitBytes, iByteTotal, sTemp);
        WriteReportLine (ItoCommas (iCacheHitBytes) + " bytes out of "
          + ItoCommas (iByteTotal) + ": " + sTemp + "\n");
      }
            
      if (iRepBUT != nBUT_none)
      {
        ShowHighLightMessage ("Sorting users' totals...");
        rUserPointers.Sort (sListRecords::nSortBytes);
        ShowHighLightMessage ("Finding full user names...");

        iFile = open (sUsersFile.c_str(), O_RDONLY);
        if (iFile > 0)
        {
          sDispLine = "";
          iNumRecords = vUserDetails.size();

          for (;;)
          {
            iRead = read (iFile, &cLetter, 1);
            if (iRead < 1)
              break;
            if ((cLetter == cEOL) || (cLetter == cCR))
            {
              if (sDispLine != "")
              {
                GetColumn (sDispLine.c_str(), 1, sTemp);
                for (iVectorCount = 0; iVectorCount < iNumRecords;
                     iVectorCount++)
                {
                  if (sTemp == vUserDetails [iVectorCount].sLoginName)
                  {
                    sDispLine.replace (0, sTemp.length() + 1, "");
                    vUserDetails [iVectorCount].sFullName = sDispLine;
                    break;
                  }
                }
                sDispLine = "";
              }
            }
            else
              sDispLine += cLetter;
          }
          close (iFile);
        }

        ShowHighLightMessage ("Done finding full user names.");
        
        sTemp = "\n" + RightJustify (ItoCommas (iUserBandWidth), nSizeCols);
        if (iRepBUT == nBUT_notveto)
          sTemp += " bytes not skipped domains";
        else
        if (iRepBUT == nBUT_all)
          sTemp += " bytes all domains";
        WriteReportLine (sTemp + "\n");

        iNumRecords = rUserPointers.vList.size();
        for (iVectorCount = 0; iVectorCount < iNumRecords; iVectorCount++)
        {
          bMadeOutput = true;
          sTemp = RightJustify (
                    ItoCommas (
                      rUserPointers.vList [iVectorCount].iBytes), nSizeCols);
          if (iRepWordHits == CSV)
            sTemp += sRepSeperator;
          else
            sTemp += ' ';
          iPointer = rUserPointers.vList [iVectorCount].iRecordNumber;
          sTemp += vUserDetails [iPointer].sLoginName;
          if (vUserDetails [iPointer].sFullName != "")  
            sTemp += " : " + vUserDetails [iPointer].sFullName;
          WriteReportLine (sTemp + "\n");
        }
      }
      
      if (iRepBDT != nBDT_none)
      {
        ShowHighLightMessage ("Sorting destination domain totals...");
        rDomainPointers.Sort (sListRecords::nSortBytes);
        ShowHighLightMessage ("Done sorting domain totals.");

        WriteReportLine ("\n");
        if (iRepBDT >= nBDT_user)
        {
          sTemp = GetLongUserName (sRepBDTuser);
          if (sTemp != "")
            WriteReportLine (sRepBDTuser + " is " + sTemp + "\n");
        }

        sTemp = RightJustify (ItoCommas (iDomainBandWidth), nSizeCols);
        if (iRepBDT >= nBDT_user)
          sTemp += " bytes user " + sRepBDTuser + " bandwidth: ";
        else
          sTemp += " bytes all users bandwidth: ";

        if (iRepBDI == nBDI_notveto)
          sTemp += "domains not skipped";
        if (iRepBDI == nBDI_all)
          sTemp += "all domains";
        WriteReportLine (sTemp + "\n");

        iNumRecords = rDomainPointers.vList.size();
        for (iVectorCount = 0; iVectorCount < iNumRecords; iVectorCount++)
        {
          bMadeOutput = true;
          sTemp = RightJustify (
                   ItoCommas (
                    rDomainPointers.vList [iVectorCount].iBytes), nSizeCols);
          if (iRepWordHits == CSV)
            sTemp += sRepSeperator;
          else
            sTemp += ' ';
          iPointer = rDomainPointers.vList [iVectorCount].iRecordNumber;
          sTemp += vDomainDetails [iPointer].sDomainName;
          WriteReportLine (sTemp + "\n");
        }
      }
    }
    catch (eDone iTemp)
    {
      iResult = iTemp;
    }
  }

  ZeroCounters();
  if (bLogOutput)
    fclose (fOutput);
    
  iTime = time (0) - iTime;

  if (wTemp)
    nodelay (wTemp, false);

  if (iResult == ioerror)
  {
    sStatusMessage = "Could not append to " + sRepFileName;
    return;
  }

  if (iResult == keybreak)
  {
    sStatusMessage = "Report canceled.";
    return;
  }

  sTemp = ItoCommas (iByteTotal) + " scanned bytes, " +
          ItoCommas (iHits) + " hits - " + ItoS (iTime) + " seconds";

  if (bLogOutput)    
  {
    if (!bMadeOutput)
    {
      sStatusMessage = "No report: " + sTemp + ".";
      return;
    }
    sCurrentReport = sRepFileName;
    sDispLine = sRepFileName + " made: " + sTemp + "; View (y/n)? ";
    if (PromptForKey (sDispLine) == 'y')
    {
      sTemp = sViewer + " " +sPathToFiles + "/" + sRepFileName;
      RunProgramInConsole (sTemp);
    }
    sStatusMessage = "Press 'n' for misc. operations you can do on "
                     + sRepFileName;
  }
  else
  {
    sStatusMessage = "Summary: " + sTemp + ".";
  }
}


// prepend a word to the search list
void PrependFindText()
{
  string sText;

  PromptForText ("find text: ", sText);
  if (sText != "")
  {
    sText += cSeperator;
    sWords = sText + sWords;
    sStatusMessage = "Now press <- or ->";
  }
}


void AppendFindText()
{
  string sText;

  PromptForText ("Find text: ", sText);
  if (sText != "")
  {
    sWords += sText;
    sWords += cSeperator;
    sStatusMessage = "Now press <- or ->";
  }
}


// size hit sub menu
void SizeHitOptions()
{
  int iCount, iSize, iTemp;
  const char *pcLetter;
  string sTemp;

  for (;;)
  {
    MyCls();
    con << "Hit sizes are:-\n\n0: <off>\n";
    pcLetter = sSizeHits.c_str();
    iCount = 1;
    for (;;)
    {
      iSize = NextCatchSize (&pcLetter);
      if (iSize <= 0)
        break;
      con << ItoS (iCount++) << ": " << ItoCommas (iSize) << "\n";
    }

    if (iSizeHitGrade == 0)
      con << "\nNo hit size set.\n";
    else
      con << "\nCurrent hit grade is " << iSizeHitGrade << ": " +
             ItoCommas (iSizeHitBytes) + " bytes\n";
    
    con << "\n-1 to -9 deletes entry, 0 to 9 sets grade.\n"
           "A large number inserts a new grade.\n"
           "Enter nothing to dismiss.\n\nEnter choice: ";
    GetText (sTemp);
    if (sTemp == "")
      return;
    iTemp = atoi (sTemp.c_str());

    if (iTemp >= 10)
    {
      if (iCount >= 10)
      {
        con << "\nNot enough room!\n\nPress a key: ";
        getch();
      }
      else
      {
        pcLetter = sSizeHits.c_str();
        sTemp = "";
        iCount = 1;
        for (;;)
        {
          iSize = NextCatchSize (&pcLetter);
          if (iTemp == iSize)
            iTemp = -1;
          if (iSize == 0)
          {
            if (iTemp >= 10)
              sTemp += " " + ItoS (iTemp);
            break;
          }
          if ((iTemp >= 10) && (iTemp < iSize))
          {
            sTemp += " " + ItoS (iTemp);
            iSizeHitGrade = iCount;
            iSizeHitBytes = iTemp;
            iTemp = -1;
          }
          sTemp += " " + ItoS (iSize);
          iCount++;
        }
        if ((sTemp != "") && (sTemp [0] == ' '))
          sTemp.replace (0, 1, "");
        sSizeHits = sTemp;
      }   
    }
    else
    if (iTemp < 0)
    {
      pcLetter = sSizeHits.c_str();
      iCount = -1;
      sTemp = "";
      for (;;)
      {
        iSize = NextCatchSize (&pcLetter);
        if (iSize <= 0)
          break;
        if (iTemp != iCount--)
          sTemp += " " + ItoS (iSize);
      }
      if ((sTemp != "") && (sTemp [0] == ' '))
        sTemp.replace (0, 1, "");
      sSizeHits = sTemp;

      iSizeHitGrade = 0;
      pcLetter = sSizeHits.c_str();
      iCount = 1;
      for (;;)
      {
        iSize = NextCatchSize (&pcLetter);
        if (iSize <= 0)
          break;
        if (iSize == iSizeHitBytes)
        {
          iSizeHitGrade = iCount;
          break;
        }
        iCount++;
      }
      if (iSizeHitGrade == 0)
        iSizeHitBytes = 0;
    }
    else
    if (sTemp == "0")
    {
      iSizeHitGrade = 0;
      iSizeHitBytes = 0;
    }
    else
    if ((iTemp >= 1) && (iTemp <= 9))
    {
      pcLetter = sSizeHits.c_str();
      iCount = 1;
      for (;;)
      {
        iSize = NextCatchSize (&pcLetter);
        if (iSize == 0)
          break;
        if (iTemp == iCount)
        {
          iSizeHitGrade = iCount;
          iSizeHitBytes = iSize;
          break;
        }
        iCount++;
      }
    }
  }
}


int CountLeadingSpaces (const string& sText)
{
  int iIndex = 0;
  int iLen = sText.length();
  
  for (iIndex = 0; iIndex < iLen; iIndex++)
    if (sText [iIndex] != ' ')
      return iIndex;
  return iLen;
}


bool IsDateTime (const string& sText)
{
  string sTemp;
  
  GetColumn (sText.c_str(), 2, sTemp);
  if (sTemp == "Sun") return true;
  if (sTemp == "Mon") return true;
  if (sTemp == "Tue") return true;
  if (sTemp == "Wed") return true;
  if (sTemp == "Thu") return true;
  if (sTemp == "Fri") return true;
  if (sTemp == "Sat") return true;
  return false;
}


bool IsTimeSpan (const string& sText)
{
  string sTemp;
  
  GetColumn (sText.c_str(), 1, sTemp);
  if (sTemp != "For")
    return false;
  return IsDateTime (sText);
}


bool IsGrandTotal (const string& sText)
{
  string sTemp;
  
  GetColumn (sText.c_str(), 2, sTemp);
  if (sTemp != "scanned")
    return false;
  GetColumn (sText.c_str(), 3, sTemp);
  if (sTemp != "bytes,")
    return false;

  return true;
}


bool IsUsersRequest (const string& sText, const string& sUser)
{
  string sTemp;
  int iCount;
  
  for (iCount = 1; iCount <= 3; iCount++)
  {
    GetColumn (sText.c_str(), iCount, sTemp);
    if (sTemp == sUser)
      return true;
  }
  return false;
}


bool FilterReport (const string& sSrc, const string& sDest,
                   const string& sUser)
{
  int iIn, iOut, iLetter, iLen, iSpaces;
  string sLine, sTime, sLastTime, sTemp;
  bool bWrite, bRet, bSplitting, bTimed, bGrand;

  iSpaces = 0;
  sTime = "";
  bSplitting = bTimed = bGrand = bRet = false;
  
  iIn = open (string (sPathToFiles + "/" + sSrc).c_str(), O_RDONLY);
  if (iIn <= 0)
    sStatusMessage = "Can't open " + sSrc + " for reading.";
  else
  {
    iOut = open (string (sPathToFiles + "/" + sDest).c_str(),
                 O_WRONLY | O_CREAT | O_TRUNC, S_IREAD | S_IWRITE);
    if (iOut <= 0)
      sStatusMessage = "Can't open " + sDest + " for writing.";
    else
    {
      sLine = "Web usage report on ";
      sTemp = GetLongUserName (sUser);
      if (sTemp == "")
        sLine += sUser + "\n\n"; 
      else
        sLine += "\"" + sUser + "\" who is " + sTemp + "\n\n"; 
      if (write (iOut, sLine.c_str(), sLine.length()) < 0)
        sStatusMessage = "Can't write to  " + sDest;

      sLine = "";
      for (;;)
      {
        iLetter = 0;
        iLen = read (iIn, &iLetter, 1);
        if ((iLen < 1) || (iLetter == cEOL))
        {
          bWrite = false;
          sLastTime = sTime;
          
          if (!bWrite)
          {
            if (IsTimeSpan (sLine))
            {
              sLine += "\n";
              bWrite = true;
            }
          }

          if (!bWrite)
          {
            if (IsDateTime (sLine))
            {
              sTime = sLine;
              iSpaces = CountLeadingSpaces (sLine);
            }
          }

          if (!bWrite)
          {
            if (IsUsersRequest (sLine, sUser))
            {
              bRet = true;
              if (bGrand)
              {
                GetColumn (sLine.c_str(), 1, sTemp);
                if (sTemp == "")
                  GetColumn (sLine.c_str(), 2, sTemp);
                if (sTemp == "")
                  sTemp = "unkown";
                sLine = "Total bytes including small requests: " + sTemp
                        + "\n";
                bWrite = true;
                iLen = 0;
              }
              else
              {
                sTemp = "";
                if (!bTimed)
                {
                  sTemp = sTime + "\n";
                  sLastTime = sTime;
                }
                sLine = sTemp + sLine;
                bWrite = true;
                bSplitting = true;
              }
            }
            else
            {
              if ((IsDateTime (sLine)) ||
                  (CountLeadingSpaces (sLine) != iSpaces))
                bSplitting = false;
            }
          }

          if (!bWrite)
          {
            if (IsGrandTotal (sLine))
            {
              sLine = "";
              bWrite = bGrand = true;
            }
          }
          
          if (bWrite || bSplitting)
          {
            sLine += "\n";
            if (write (iOut, sLine.c_str(), sLine.length()) < 0)
              sStatusMessage = "Can't write to  " + sDest;
          }
          sLine = "";
        }
        else
          sLine += char (iLetter);
        if (iLen < 1)
          break;
      }
      close (iOut);
    }
    close (iIn);
  }
  return bRet;
}


// menu
void SearchOptions()
{
  int iKey = 0, iPause;
  const char* pcLetter;
  char cLetter;
  string sCatch, sTemp, sFocus;
  
  for (;;)
  {
    MyCls();

    con << "Search Options: current words are:-\n\n";
    pcLetter = sWords.c_str();
    while ((cLetter = *pcLetter++) != 0)
    {
      if (cLetter == cSeperator)
        cLetter = ' ';
      addch (cLetter);
    }

    if (iSizeHitGrade == 0)
      sCatch = "<off>";
    else
      sCatch = ItoS (iSizeHitGrade) + " (" +
               ItoCommas (iSizeHitBytes) + ")";

    switch (iRepFocus)
    {
      case no_focus:
        sFocus = "no focus";
        break;

      case user:
        sFocus = "focus on user " + sRepFocus;
        break;

      default:
        sFocus = "<error>";
        break;
    }
     
    con << "\n\nIn the case of skips (\"!\"s) first words "
           "have higher preference.\n\n"
           "Commands:\n\n"
           "n.  null words\n"
           "r.  reload words\n"
           "s.  save words\n"
           "c.  change request hit size from " << sCatch << "\n"
           "f.  focus: " << sFocus << "\n"
           "q.  quit this screen\n\n"
           "Your choice: ";
    refresh();

    iKey = getch();
    iPause = 0;
    switch (iKey)
    {
      case 'n':
        sWords = "";
        con << "\n\nAll words gone.\n";
        iPause = 1;
        break;

      case 'r':
        if (LoadWords())
          con << "\n\nWords reset.\n";
        else
          con << "\n\nOpps, problem loading words.\n";
        iPause = 1;
        break;

      case 's':
        if (SaveWords())
          con << "\n\nWords saved.\n";
        else
          con << "\n\nOpps, problem saving words.\n";
        iPause = 1;
        break;

      case 'c':
        SizeHitOptions();
        break;

      case 'f':
        MyCls();
        con << "Focus options:\n\n"
               "1.  no focus\n"
               "2.  a user\n";
        sFocus = "";
        GetLine (iLinePos);
        if (NullText (pcReqBuff))
          sTemp = "";
        else
        {
          sFocus = GetUserName (pcReqBuff, false);
          if (sFocus != "")
            con << "3.  focus on user " << sFocus << "\n";
        }
        con << "\nYour choice: ";
        switch (getch())
        {
          case '1':
            iRepFocus = no_focus;
            break;

          case '2':
            con << "\n\nEnter user name: ";
            GetText (sTemp);
            if (sTemp == "")
              iRepFocus = no_focus;
            else
            {
              StrLwr (sTemp);
              sRepFocus = sTemp;
              iRepFocus = user;
            }
            break;

          case '3':
            if (sFocus != "")
            {
              sRepFocus = sFocus;
              iRepFocus = user;
            }
            break;
        }
        break;

      case 'q':
        return;
    }
    if (iPause)
    {
      con << "\nPress any key.";
      getch();
    }
  }
}


void CommonOptions()
{
  int iKey, iTemp;
  string sTemp;
  
  for (;;)
  {
    MyCls();
    con << "Common Options\n\n"
           "These affect both the browsing window and reports.\n"
           "\ne.  keep target/extension/file type: " << bRepKeepExt <<
           "\nn.  IP number instead of null user: " << bLookupIP <<
           "\na.  use alias file to map IPs to names: " << bAliases <<
           "\nw.  width of user name field: eg 8 (for names) "
           "or 15 (for IPs): " << iUserNameLen <<
           "\nq.  quit this menu\n\n"
           "Your choice: ";
    iKey = getch();
    con << "\n\n";
    switch (iKey)
    {
      case 'q':
        return;

      case 'e':
        bRepKeepExt = !bRepKeepExt;
        break;

      case 'n':
        bLookupIP = !bLookupIP;
        break;

      case 'a':
        bAliases = !bAliases;
        if (bAliases && vAliasList.empty())
        {
          con << "Get a working aliases file first.\nPress a key. ";
          getch();
          bAliases = false;
        }
        break;

      case 'w':
        con << "New width: ";
        GetText (sTemp);
        iTemp = atoi (sTemp.c_str());
        if ((iTemp >= 1) && (iTemp <= 64))
          iUserNameLen = iTemp;
        break;

      default:
        con << "Bad option. Press a key. ";
        getch();
    }
  }
}


void MiscLogOptions()
{
  int iKey = 0, iPause;
  string sTemp, sFile, sFilter, sCheck, sTemp1, sTemp2, sUser, sDest;
  bool bGood;
  
  for (;;)
  {
    MyCls();
    con << "Misc Log Options\n\n"
           "Current report is: " << sCurrentReport << "\n"
           "Current filtered report is: " << sRepFilter << "\n"
           "Current email is: " << sCurrentEmail << "\n\n"
           "Commands:\n\n"
           "c.  change current report\n"
           "C.  change filter report\n"
           "l.  list reports\n"
           "d.  delete the report\n"
           "v.  view current report with " << sViewer << "\n"
           "V.  view filtered report\n"
           "f.  filter the report for just one user\n"
           "e.  change current email\n"
           "m.  mail current report to current email with Pine\n"
           "s.  show built in log filenames\n"
           "q.  quit this menu\n\n"
           "Your choice: ";
    iKey = getch();
    con << "\n\n";
    iPause = 0;
    sFile = sPathToFiles + "/" + sCurrentReport;
    sFilter = sPathToFiles + "/" + sRepFilter;
    switch (iKey)
    {
      case 'c':
        con << "Enter new current report: ";
        GetFileName (sTemp);
        if (sTemp != "")
          sCurrentReport = sTemp;
        break;

      case 'C':
        con << "Enter filtered report name: ";
        GetFileName (sTemp);
        if (sTemp != "")
          sRepFilter = sTemp;
        break;

      case 'l':
        sTemp = "( cd " + sPathToFiles + " ; " +
                "echo -e \"Current reports:\n\" ; " +
                "ls -l *" + sReportExt + " ) | " + sViewer;
        RunProgramQuietly (sTemp);
        break;

      case 'd':
        if (sCurrentReport == "")
        {
          con << "No current report to delete.\n";
          iPause = 1;
          break;
        }
        sTemp = "rm " + sFile;
        RunProgramQuietly (sTemp);
        sCurrentReport = "";
        break;

      case 'v':
        if (GetFileSize (sFile.c_str()) == 0)
        {
          con << "Empty file :(\n";
          iPause = 1;
        }
        else
        {
          sTemp = sViewer + " " + sFile;
          RunProgramInConsole (sTemp);
        }
        break;

      case 'V':
        if (GetFileSize (sFilter.c_str()) == 0)
        {
          con << "Empty file :(\n";
          iPause = 1;
        }
        else
        {
          sTemp = sViewer + " " + sFilter;
          RunProgramInConsole (sTemp);
        }
        break;

      case 'f':
        if (GetFileSize (sFile.c_str()) == 0)
        {
          con << "No or empty file :(\n";
          iPause = 1;
          break;
        }

        MyCls();
        con << "Filter a report leaving one user.\n\n"
            << "Enter new report name: ";
        GetFileName (sDest, &bGood);
        if (!bGood)
          break;
        con << "\n";
        
        if (sDest == sCurrentReport)
        {
          con << "Destination report must be different to source.\n\n";
          iPause = 1;
          break;
        }

        if (iRepFocus == user)
          sTemp1 = sRepFocus;
        else
          sTemp1 = "";
        GetLine (iLinePos);
        if (NullText (pcReqBuff))
          sTemp2 = "";
        else
          sTemp2 = GetUserName (pcReqBuff, false);
                
        con << "Filter report for:-\n\n"
               "1.  focus user " << sTemp1 << "\n"
               "2.  current user " << sTemp2 << "\n"
               "3.  specify a different user\n\n"
               "Your choice: ";
        iKey = getch();
        con << "\n\n";
        sUser = "";
        switch (iKey)
        {
          case '1':
            sUser = sTemp1;
            break;

          case '2':
            sUser = sTemp2;
            break;

          case '3':
            con << "Enter user name: ";
            GetText (sUser);
            con << "\n";
            StrLwr (sUser);
            break;
        }
        if (sUser != "")
        {
          con << "Working... ";
          sStatusMessage = "";
          bGood = FilterReport (sCurrentReport, sDest, sUser);
          if (bGood)
          {
            sRepFilter = sDest;
            con << "\n\nReport " + sDest + " made: view (y/n)?";
            if (getch() == 'y')
            {
              sTemp = sViewer + " " + sPathToFiles + "/" + sDest;
              RunProgramInConsole (sTemp);
            }            
          }
          else
          {
            con << "\n\nEmpty report " << sDest << ":\n"
                << sStatusMessage << "\n";
            iPause = 1;
          }
          sStatusMessage = "";
        }
        break;

      case 'e':
        con << "Enter new email address: ";
        GetText (sTemp);
        if (sTemp != "")
        {
          sCheck = CheckEmail (sTemp);
          if (sCheck != "")
          {
            con << "\nSorry, no illegal characters.\n";
            iPause = 1;
          }
          else
            sCurrentEmail = sTemp;
        }
        break;

      case 'm':
        if (GetFileSize (sFile) == 0)
        {
          con << "Empty file :(\n";
          iPause = 1;
          break;
        }
        if (sCurrentEmail == "")
        {
          con << "No email address.\n";
          iPause = 1;
          break;
        }
       con << "You are about to send the report using the\n"
              "email program Pine. Press <ctrl>-x in Pine to\n"
              "send it.\n"
              "\n"
              "Press any key to begin";
        getch();
        sTemp = "pine -attach " + sFile + " " + sCurrentEmail;
        RunProgramInConsole (sTemp);
        break;

      case 's':
        MyCls();
        con << "Log files:\n\n"
            << szLabel1 << " -> " << sLogFile1 << "\n"
            << szLabel2 << " -> " << sLogFile2 << "\n"
            << szLabel3 << " -> " << sLogFile3 << "\n"
            << "\nTo adjust these do this:\n\n"
            << "cd ~/.squidview\n"
            << "rm log1\n"
            << "ln -s /var/whatever/access.log log1\n";
        iPause = 1;
        break;

      case 'q':
        return;
    }
    if (iPause)
    {
      con << "\nPress any key.";
      getch();
      RunProgramInConsole ("clear");
    }
  }
}


// put some text at the end of a string (overwrite)
void ShowMsg (string& sLine, const string& sMsg)
{
  int iLLen, iMLen;

  iLLen = sLine.length();
  iMLen = sMsg.length();
  if (iMLen > iLLen)
    return;
  sLine.replace (iLLen - iMLen, iMLen, "");
  sLine += sMsg;
}


// go to a certain point in the log file
void GotoPercentage()
{
  string sText;
  double dPercent;

  PromptForText ("Goto percentage: ", sText);
  if (sText != "")
  {
    dPercent = strtod (sText.c_str(), 0);
    if ((dPercent >= 0) && (dPercent <= 100))
      CentreScreen (GetLine (tFilePos (iLastLinePos * dPercent / 100)));
  }
}


// next position ahead in log
inline tFilePos ForwardNextLeap (tFilePos iStart, tFilePos iInc)
{
  tFilePos iNext, iMiddle;
  
  if (iStart >= iLastLinePos)
    return -1;
    
  iNext = iStart + iInc;
  if (iNext >= iLastLinePos)
    return iLastLinePos;

  iMiddle = GetPrevLine (iNext);
  if (iMiddle <= iStart)
  {
    iMiddle = GetLine (iStart);
    if ((iMiddle == 0) || (iMiddle >= iLastLinePos))
      return -1;
  }
  return iMiddle;
}


// next position backwards in log
inline tFilePos BackwardNextLeap (tFilePos iStart, tFilePos iInc)
{
  tFilePos iNext, iMiddle;
  
  if (iStart <= 0)
    return -1;
    
  iNext = iStart - iInc;
  if (iNext <= 0)
    return 0;

  iMiddle = GetPrevLine (iNext);
  if (iMiddle <= 0)
    iMiddle = 0;
  
  return iMiddle;
}


// seconds of highlighted line
time_t CalcCurrentSecs()
{
  string sBuff;
  time_t tSecs;
  
  GetLine (iLinePos);
  if (NullText (pcReqBuff))
    return 0;

  tSecs = GetTimeNumber (pcReqBuff);
  return tSecs;
}


// this is necessary to get around daylight savings issues
time_t Get4am (time_t tSecs)
{
  struct tm* tmTable;
  
  tmTable = localtime (&tSecs);
  tmTable->tm_sec = 0;
  tmTable->tm_min = 0;
  tmTable->tm_hour = 4;
  return mktime (tmTable);
}


// nuke minutes and hours of a day
time_t Get12am (time_t tSecs)
{
  struct tm* tmTable;
  
  tmTable = localtime (&tSecs);
  tmTable->tm_sec = 0;
  tmTable->tm_min = 0;
  tmTable->tm_hour = 0;
  return mktime (tmTable);
}


// return day of month
inline int CalcDayFromSecs (time_t tSecs)
{
  struct tm* tmTable;

  if (tSecs == 0)
    return 0;
    
  tmTable = localtime (&tSecs);
  return tmTable->tm_mday;
}


// having the above and lower bounds in log, zoom in
void SequenceJump (tFilePos iBelow, tFilePos iAbove, time_t iSecs)
{
  tFilePos iNext;

  ShowHighLightMessage ("Sequencing...");
  
  for (;;)
  {
    iNext = GetLine (iBelow);
    if (GetTimeNumber (pcReqBuff) >= iSecs)
    {
      CentreScreen (iBelow);
      return;
    }
    if (iNext >= iAbove)
    {
      CentreScreen (iAbove);
      return;
    }
    iBelow = iNext;
  }
}


// split in two until time in range, then zoom
void SplitJump (time_t iSecs)
{
  tFilePos iBelow, iAbove, iMiddle;
  time_t iTry;
  
  iBelow = 0;
  iAbove = iLastLinePos;

  ShowHighLightMessage ("Splitting pointer...");
  
  for (;;)
  {
    if (iAbove <= iBelow)
    {
      CentreScreen (iBelow);
      return;
    }
    if (iAbove - iBelow < 100000)
    {
      SequenceJump (iBelow, iAbove, iSecs);
      return;
    }

    iMiddle = (iBelow + iAbove) >> 1;
    iMiddle = GetLine (iMiddle);
    for (;;)
    {
      iMiddle = GetLine (iMiddle);
      iTry = GetTimeNumber (pcReqBuff);
      if (iTry != 0)
        break;
      if (iMiddle >= iAbove)
      {
        sStatusMessage = "Problem in jump :(";
        return;
      }
    }
    if (iTry < iSecs)
      iBelow = iMiddle;
    else
      iAbove = iMiddle;
  }
}


// move along to a certain day
void JumpToDayForward()
{
  int iDay, iCount;
  time_t tCReq;
  string sBuffer;

  if (iLogFileSize == 0)
    return;

  tCReq = CalcCurrentSecs();
  if (tCReq == 0)
  {
    sStatusMessage = "Bad day to start with.";
    return;
  }

  PromptForText ("Jump forward to day of month: ", sBuffer);
  if (sBuffer == "")
    return;
  iDay = atoi (sBuffer.c_str());
  if ((iDay < 1) || (iDay > 31))
    return;

  ShowHighLightMessage ("Finding day...");

  tCReq = Get4am (tCReq);
  if (iDay == CalcDayFromSecs (tCReq))
    tCReq += nSecsInDay;

  iCount = 0;
  for (;;)
  {
    if (CalcDayFromSecs (tCReq) == iDay)
      break;
    if (++iCount > 100)
    {
      sStatusMessage = "Couldn't find day";
      return;
    }
    tCReq += nSecsInDay;
  }

  tCReq = Get12am (tCReq);

  SplitJump (tCReq);
  return;
}


// move backward to a certain day
void JumpToDayBackward()
{
  string sBuffer;
  int iDay, iCount;
  time_t tCReq;

  if (iLogFileSize == 0)
    return;

  tCReq = CalcCurrentSecs();
  if (tCReq == 0)
  {
    sStatusMessage = "Bad day to start with.";
    return;
  }

  PromptForText ("Jump backward to day of month: ", sBuffer);
  if (sBuffer == "")
    return;
  iDay = atoi (sBuffer.c_str());
  if ((iDay < 1) || (iDay > 31))
    return;

  ShowHighLightMessage ("Finding day...");

  tCReq = Get4am (tCReq);
  if (iDay == CalcDayFromSecs (tCReq))
    tCReq -= nSecsInDay;

  iCount = 0;
  for (;;)
  {
    if (CalcDayFromSecs (tCReq) == iDay)
      break;
    if (++iCount > 100)
    {
      sStatusMessage = "Couldn't find day";
      return;
    }
    tCReq -= nSecsInDay;
  }

  tCReq = Get12am (tCReq);

  SplitJump (tCReq);
  return;
}


void ChangeLog (int iNewLog)
{
  string sTemp;
  int iLen;
  int iBase;
  
  switch (iNewLog)
  {
    case 1:
      pszCurrentLog = sLogFile1.c_str();
      pszCurrentName = szLabel1;
      iCurrentLog = iNewLog;
      break;

    case 2:
      pszCurrentLog = sLogFile2.c_str();
      pszCurrentName = szLabel2;
      iCurrentLog = iNewLog;
      break;

    case 3:
      pszCurrentLog = sLogFile3.c_str();
      pszCurrentName = szLabel3;
      iCurrentLog = iNewLog;
      break;
  }

  if (iNewLog >= 1)
    aTally.ZeroAll();

  CalcLastPage();
  iRepStart = 0;
  iRepEnd = 0;
  if (iMonitorMode)
  {
    iPagePos = iLastPage;
    iLinesDown = iMaxLinesDown;
  }
  else
  {
    iPagePos = 0;
    iLinesDown = 0;
  }

  GetLine (0);
  iBase = 0;
  
  GetColumn (pcReqBuff, 5, sTemp);
  iLen = sTemp.length();
  if ((iLen > 2) && (sTemp [iLen - 1] == ':'))
    iBase = 5;

  iTimeCol = iBase + 1;
  iIPCol = iBase + 3;
  iCacheHitCol = iBase + 4;
  iSizeCol = iBase + 5;
  iTargetCol = iBase + 7;

  GetColumn (pcReqBuff, iBase + 3, sTemp);
  if ((sTemp == "") || (sTemp [0] < 'A'))
    iUserCol = iBase + 8;
  else
    iUserCol = iBase + 3;
}


void ViewSelectedLine (tFilePos iLogPos)
{
  string sUserName, sLongName;
  int iKey = 0;
  bool bLegal;

  MyCls();
  GetLine (iLogPos);
  if (NullText (pcReqBuff))
  {
    con << "Line not available.\n";
    bLegal = false;
  }
  else
    bLegal = true;

  if (bLegal)
  {
    con << pcReqBuff << "\n\n"
        << "Request size = " << ItoCommas (GetRequestSize (pcReqBuff))
        << "\n\nLooking for user...";
    sUserName = GetUserName (pcReqBuff, false);
    sLongName = GetLongUserName (sUserName);
    if (sLongName == "")
      con << "not found.\n";
    else
      con << "\n\n" << sUserName << " is " << sLongName << "\n";
  }

  con << "\nPress q:";
  refresh();

  while (iKey != 'q')
    iKey = getch();

  MyCls();
}


int CheckUpdate()
{
  static int iScreenVal = -1;
  int iTemp;
  int ReCalc;

  ReCalc = 0;
  iTemp = (COLS * 65536) + LINES;
  if (iScreenVal != iTemp)
  {
    erase();
    iScreenVal = iTemp;
    iMainLines = LINES - 1;
    ReCalc = 2;
  }
  if (GetFileSize (pszCurrentLog) != iLogFileSize)
    ReCalc++;
  if (ReCalc)
  {
    CalcLastPage();
    if (ReCalc >= 2)
    {
      ChangeLog (-1);
      return 1;
    }
  }
  return 0;
}


class SettingsIO
{
  public:
    SettingsIO();
    void Save();
    void Load();

  private:
    FILE* fIO;
    bool bSave;
    string sLoadName, sLoadValue;
    bool bError;

    void Write (string sText);
    void IOsetting (string sName, string sValue);
    void IObool (const char*, bool&);
    void IOint (const char*, int&);
    void IOfloat (const char*, float&);
    void IOstring (const char*, string&);
    void EachOne();
};


SettingsIO::SettingsIO()
{
  sSetFileName = "squidview.conf";
}


void SettingsIO::Save()
{
  bSave = true;
  bError = false;
  fIO = fopen (sSetFileName.c_str(), "w");
  if (fIO == NULL)
  {
    sStatusMessage = string ("Could not write to ") + sSetFileName;
    return;
  }
  Write ("# this complete file is reset on every clean exit\n");
  EachOne();
  fclose (fIO);
}


void SettingsIO::Load()
{
  int iFile, iRead, iLetter, iCount, iLength;
  char cLetter;
  bool bGotEquals;
  string sLine;

  bSave = false;
  bError = false;

  iFile = open (sSetFileName.c_str(), O_RDONLY);
  if (iFile <= 0)
  {
    sStatusMessage = string ("Could not read ") + sSetFileName;
    return;
  }

  sLine = "";
  iRead = 1;
  while (iRead == 1)
  {
    iLetter = 0;
    iRead = read (iFile, &iLetter, 1);
    if (iRead == 1)
    {
      if ((iLetter == cEOL) || (iLetter == cCR))
      {
        iLength = sLine.length();
        if ((iLength > 0) && (sLine [0] != '#'))
        {
          sLoadName = "";
          sLoadValue = "";
          bGotEquals = false;
          for (iCount = 0; iCount < iLength; iCount++)
          {
            cLetter = sLine [iCount];
        	if (bGotEquals)
        	  sLoadValue += cLetter;
        	else
        	if (cLetter == '=')
        	  bGotEquals = true;
        	else
        	  sLoadName += cLetter;
          }
          EachOne();
        }
        sLine = "";
      }
      else
        sLine += char (iLetter);
    }
  }
  close (iFile);
}


void SettingsIO::Write (string sText)
{
  int iTemp;
  
  if (bError)
    return;
      
  iTemp = sText.length();
  if (int (fwrite (sText.c_str(), 1, iTemp, fIO)) == iTemp)
    return;
  sStatusMessage = string ("Could not append to ") + sSetFileName;
  bError = true;
}



void SettingsIO::IOsetting (string sName, string sValue)
{
  if (bError)
    return;
      
  if (bSave)
    Write (sName + "=" + sValue + "\n");
}


void SettingsIO::IObool (const char* szText, bool& bVariable)
{
  string sTemp;
  
  if (bSave)
  {
    sTemp = bVariable == true ? "yes" : "no";
    IOsetting (szText, sTemp);
  }
  else
  if (sLoadName == szText)
    bVariable = sLoadValue == "yes" ? true: false;
}


void SettingsIO::IOint (const char* szText, int& iVariable)
{
  if (bSave)
    IOsetting (szText, ItoS (iVariable));
  else
  if (sLoadName == szText)
    iVariable = atoi (sLoadValue.c_str());
}


void SettingsIO::IOfloat (const char* szText, float& fVariable)
{
  char szTemp [32];
  
  if (bSave)
  {
    snprintf (szTemp, sizeof (szTemp), "%f", fVariable);
    IOsetting (szText, szTemp);
  }
  else
  if (sLoadName == szText)
    fVariable = atof (sLoadValue.c_str());
}


void SettingsIO::IOstring (const char* szText, string& sVariable)
{
  if (bSave)
    IOsetting (szText, sVariable);
  else
  if (sLoadName == szText)
    sVariable = sLoadValue;
}


// store these variables in config file
void SettingsIO::EachOne()
{
  IOstring ("sRepFileName", sRepFileName);
  IOstring ("sRepFilter", sRepFilter);
  IOstring ("sRepTitle", sRepTitle);
  IObool ("bRepKeepExt", bRepKeepExt);
  IOint ("iRepColumns", iRepColumns);
  IObool ("bRepSplit", bRepSplit);
  IObool ("bRepShowSize", bRepShowSize);
  IObool ("bRepCacheReport", bRepCacheReport);
  IOstring ("sRepSeperator", sRepSeperator);
  IOint ("iRepCSVcols", iRepCSVcols);
  IOint ("iRepWordHits", iRepWordHits);
  IOint ("iRepBUT", iRepBUT);   
  IOint ("iRepBDT", iRepBDT);   
  IOint ("iRepBDI", iRepBDI);   
  IOint ("iRepFocus", iRepFocus);   
  IOstring ("sRepFocus", sRepFocus);
  IOstring ("sRepBDTuser", sRepBDTuser);
  IOstring ("sViewer", sViewer);
  IOstring ("sCurrentReport", sCurrentReport);
  IOstring ("sCurrentEmail", sCurrentEmail);
  IOint ("iSizeHitGrade", iSizeHitGrade);
  IOint ("iSizeHitBytes", iSizeHitBytes);
  IOstring ("sSizeHits", sSizeHits);
  IOint ("iCurrentLog", iCurrentLog);
  IOint ("iMonitorMode", iMonitorMode);
  IObool ("bLookupIP", bLookupIP);
  IObool ("bAliases", bAliases);
  IOint ("iUserNameLen", iUserNameLen);
  IOint ("iRepFast", iRepFast);
  IObool ("bRepMySort", bRepMySort);

  IOint ("iTallyHP", aTally.iHistoryPeriod);
  IOfloat ("iTallyFade", aTally.fFadeFactor);
  IOfloat ("iTallyEntry", aTally.fEntryFactor);
  IObool ("iTallyMonitor", aTally.bUsersMonitorMode);
  IObool ("iTallyDenies", aTally.bAcceptDenies);
}


void ViewHowTo()
{
  if (GetFileSize (sHowToLink) <= 0)
  {
    MyCls();
    con << "I can't find the howto for this program.\n\n"
        << "There should be this link:\n\n"
        << sHowToLink << "\n\n"
        << "pointing to:\n\n"
        << SHAREDIR << "/" << szHowToFile << "\n\n"
        << "You can set the correct link manually.\n"
        << "Press q:";
    for (;;)
    {
      if (getch() == 'q')
        return;
    }
  }
  else
    RunProgramInConsole (sViewer + " " + sHowToLink);
}


int WaitKeyOrLog (tFilePos iLastSize)
{
  fd_set fdsInput;
  struct timeval tvTimeOut;
  int iTemp;

  for (;;)
  {
    FD_ZERO (&fdsInput);
    FD_SET (0, &fdsInput);
    tvTimeOut.tv_sec = 3;
    tvTimeOut.tv_usec = 0;
    iTemp = select (1, &fdsInput, 0, 0, &tvTimeOut);
    if (iTemp)
      return getch();
    else
      if (GetFileSize (pszCurrentLog) != iLastSize)
        return 0;
  }
}


string CalcIdle (time_t iLastTime)
{
  int iTime;
  
  iTime = time (0) - iLastTime;
  if (iTime < 0)
    return "!";
  if (iTime < 60)
    return ItoS (iTime) + "s";
    
  iTime = iTime / 60;
  if (iTime < 60)
    return ItoS (iTime) + "m";

  iTime = iTime / 60;
  if (iTime < 60)
    return ItoS (iTime) + "h";
    
  iTime = iTime / 24;
  return ItoS (iTime) + "d";
}


time_t TimeLapse (time_t iInstant = 0)
{
  if (iInstant == 0)
    return time (0);
  else
    return time (0) - iInstant;
}


bool EnterUserHistory (const string& sLoginName)
{
  cUserHistory aUH;
  
  return aUH.Run (sLoginName);
}


bool cUserHistory::Run (const string& sLoginName)
{
  sUser = sLoginName;
  iScreenStart = iSelectedRow = 0;
  MyCls();
  CalcScreen();
  iNorthMark = -1;
  iSouthMark = iSouthMarkEnd = iOneUserFileSize;
  AddHistory();
  return Browse();
}

  
void cUserHistory::CalcScreen()
{
  tFilePos iCurrent;
  
  iCurrent = GetFileSize (pszCurrentLog);
  if (iCurrent < iOneUserFileSize)
    ZeroAll();
  else
    iLogFileSize = iOneUserFileSize = iCurrent;
  iScreenHeight = LINES - 2;
  iTotalRows = vRequests.size();
  iSelectedAddress = iScreenStart + iSelectedRow;
  iLastScreenRow = iTotalRows - iScreenHeight;
  if (iLastScreenRow < 0)
    iLastScreenRow = 0;
}


void cUserHistory::ZeroAll()
{
  while (!vRequests.empty())
    vRequests.pop_back();
  iScreenStart = iSelectedRow = 0;
  iNorthMark = 0;
  iSouthMark = iSouthMarkEnd = 0;
  iLogFileSize = iOneUserFileSize = 0;
}


bool cUserHistory::Browse()
{
  int iKey;
  string sTemp;
  
  for (;;)
  {
    CalcScreen();
    DisplayHistory();
    refresh();
    iKey = WaitKeyOrLog (iOneUserFileSize);
    CalcScreen();
     
    switch (iKey)
    {
      case KEY_UP:
      case '8':
        if (iSelectedRow > 0)
          iSelectedRow--;
        else
        {
          if (iScreenStart > 0)
            iScreenStart--;
          else
            AddHistoryNorth();
        }
        break;

      case KEY_DOWN:
      case '2':
        if (iSelectedRow < iScreenHeight - 1)
        {
          if (iSelectedAddress < iTotalRows - 1)
            iSelectedRow++;
          else
          {
            if (AddHistorySouth() > 0)
            {
              if (vRequests.size() == 1)
                iSelectedRow = 0;
              else
                iSelectedRow++;
            }
          }
        }
        else
        {
          if (iScreenStart < iLastScreenRow)
            iScreenStart++;
          else
          {
            if (AddHistorySouth() > 0)
              iScreenStart++;
          }
        }
        break;

      case KEY_PPAGE:
      case '9':
        if (iScreenStart == 0)
          iSelectedRow = 0;
        else
        {
          iScreenStart = iScreenStart - iScreenHeight;
          if (iScreenStart < 0)
            iScreenStart = 0;
        }
        break;

      case KEY_NPAGE:
      case '3':
        if (iScreenStart >= iLastScreenRow)
        {
          iScreenStart = iLastScreenRow;
          iSelectedRow = iScreenHeight - 1;
          if (iSelectedRow > iTotalRows - 1)
            iSelectedRow = iTotalRows - 1;
        }
        else
        {
          iScreenStart += iScreenHeight;
          if (iScreenStart > iLastScreenRow)
            iScreenStart = iLastScreenRow;
        }
        break;
      
      case KEY_HOME:
      case '7':
        iScreenStart = iSelectedRow = 0;
        break;
      
      case KEY_END:
      case '1':
        iScreenStart = iLastScreenRow;
        if (iTotalRows < iScreenHeight)
          iSelectedRow = iTotalRows - 1;
        else
          iSelectedRow = iScreenHeight - 1;
        if (iSelectedRow < 0)
          iSelectedRow = 0;
        break;

      case 'v':
        if (iSelectedAddress < iTotalRows)
          ViewSelectedLine (vRequests [iSelectedAddress]);
        break;

      case 'u':
        PromptForText ("Switch to user: ", sTemp);
        if (sTemp != "")
        {
          MyCls();
          sUser = sTemp;
          ZeroAll();
        }
        break;
      
      case 'Z':
        ZeroAll();
        break;

      case 'h':
        DisplayHelp();
        break;

      case 'r':
        ViewHowTo();
        break;

      case 'q':
        return false;

      case 24:   // ctrl-x
        return true;
    }
  }
}


void cUserHistory::AddHistory()
{
  int iCount;
  time_t iInstant;
  
  CalcScreen();
  iCount = 0;
  iInstant = TimeLapse (0);
  
  for (;;)
  {
    if (AddHistoryNorth() == 0)
      break;
    if (++iCount >= iScreenHeight)
      break;
    if (TimeLapse (iInstant) >= 2)
      break;
  }

  CalcScreen();
}


int cUserHistory::AddHistoryNorth()
{
  tFilePos iCurrentPos;
  string sComment;
  int iCount, iReturn = 0, iSize;
  bool bFound;

  if (iNorthMark == 0)
    return 0;
  
  if (iNorthMark > 0)
    iCurrentPos = iNorthMark;
  else
  {
    if (vRequests.size() == 0)
      iCurrentPos = iOneUserFileSize;
    else
      iCurrentPos = vRequests [0];
  }

  sComment = "History of " + sUser;
  iCount = 0;
  GoForNoDelay();
  bFound = false;
      
  for (;;)
  {
    iNorthMark = GetPrevLine (iCurrentPos);
    if (iNorthMark == 0)
    {
      iReturn = 0;
      break;
    }

    if (!NullText (pcReqBuff))
      if (GetUserName (pcReqBuff, false) == sUser)
      {
        bFound = true;
        break;
      }

    if (++iCount > nMaxNorth)
    {
      iReturn = -1;
      break;
    }
      
    if (CheckInterrupt())
    {
//      bUsersMonitorMode = false;
      iReturn = -2;
      break;
    }
    if (CheckTicker())
    {
      UpdateTicker (sComment.c_str(), iNorthMark, iOneUserFileSize);
    }
    
    iCurrentPos = iNorthMark;
  }

  if (wTemp)
    nodelay (wTemp, false);

  if (!bFound)
    return iReturn;

  iSize = int (vRequests.size());
  if (iSize < nMaxRows)
    vRequests.push_back (iNorthMark);
  else
  {
    iSouthMark = vRequests [iSize - 2];
    iSouthMarkEnd = vRequests [iSize - 1];
  }
  for (iCount = vRequests.size() - 1; iCount > 0; iCount--)
    vRequests [iCount] = vRequests [iCount - 1];
  vRequests [0] = iNorthMark;
  return 1;
}


int cUserHistory::AddHistorySouth()
{
  tFilePos iCurrentPos, iNextPos;
  string sComment;
  int iCount, iReturn = 0, iSize;
  bool bFound;

  if (iSouthMarkEnd >= iOneUserFileSize)
    return 0;
  
  iCurrentPos = iSouthMarkEnd;

  sComment = "History of " + sUser;
  iCount = 0;
  GoForNoDelay();
  bFound = false;
      
  for (;;)
  {
    iNextPos = GetLine (iCurrentPos);
    if (iNextPos <= iCurrentPos)
    {
      iReturn = 0;
      iNextPos = iCurrentPos;
      break;
    }

    if (!NullText (pcReqBuff))
      if (GetUserName (pcReqBuff, false) == sUser)
      {
        bFound = true;
        break;
      }

    if (++iCount > nMaxSouth)
    {
      iReturn = -1;
      break;
    }
      
    if (CheckInterrupt())
    {
//      bUsersMonitorMode = false;
      iReturn = -2;
      break;
    }
    if (CheckTicker())
    {
      UpdateTicker (sComment.c_str(), iNextPos, iOneUserFileSize);
    }
    
    iCurrentPos = iNextPos;
  }

  if (wTemp)
    nodelay (wTemp, false);

  iSouthMarkEnd = iNextPos;

  if (!bFound)
    return iReturn;
  
  iSouthMark = iCurrentPos;
  iSize = int (vRequests.size());
  if (iSize < nMaxRows)
  {
    vRequests.push_back (iSouthMark);
    return 1;
  }
  else
  {
    for (iCount = 0; iCount < iSize - 1; iCount++)
      vRequests [iCount] = vRequests [iCount + 1];
    vRequests [iSize - 1] = iSouthMark;
    iNorthMark = vRequests [0];
    return 0;
  }
}


void cUserHistory::DisplayHistory()
{
  int iScreenY, iCount, iRow, iColumns;
  string sLine, sFlags, sTarget, sTemp;
  bool bInverse;
  time_t iTime;

  iColumns = COLS - 1;
  sLine = "One user history: " + sUser;
  Columnize (sLine, iColumns);
  move (0, 0);
  attron (A_REVERSE);
  con << sLine;
  
  iRow = iScreenStart;
  iScreenY = 1;
  iTime = 0;
  
  for (iCount = 0; iCount < iScreenHeight; iCount++)
  {
    sLine = "";
    bInverse = false;
    if (iRow < iTotalRows)
    {
      if (GetLine (vRequests [iRow]) > vRequests [iRow])
      {
        GetRequestFlags (pcReqBuff, sFlags);
        GetColumn (pcReqBuff, iTargetCol, sTarget);
        RemoveSlashes (sTarget);
        sLine = sFlags + " " + sTarget;
        KeepExtension (sLine, iColumns);
        if (iRow == iSelectedAddress)
        {
          iTime = GetTimeNumber (pcReqBuff);
          bInverse = true;
        }
      }
    }
    Columnize (sLine, iColumns);
    move (iScreenY, 0);
    if (bInverse)
        attron (A_REVERSE);
    else
        attroff (A_REVERSE);
    con << sLine;
    iScreenY++;
    iRow++;
  }

  CalcPercentage (iNorthMark, iOneUserFileSize, sTemp);
  NoLeadingSpaces (sTemp);
  sLine = "(" + sTemp;
  CalcPercentage (iSouthMarkEnd, iOneUserFileSize, sTemp);
  NoLeadingSpaces (sTemp);
  sLine += " to " + sTemp;
  CalcPercentage (iSelectedAddress,
                  iTotalRows == 0 ? 0 : iTotalRows - 1, sTemp);
  NoLeadingSpaces (sTemp);
  sLine += ") " + sTemp;
  
  if (iTime > 0)
  {
    CalcTime (iTime, sTemp);
    RemoveSeconds (sTemp);
    sLine += " " + sTemp;
  }
  
  Columnize (sLine, iColumns);
  ShowMsg (sLine, " | h = help");
  move (iScreenY, 0);
  attron (A_REVERSE);
  con << sLine;
}



void cUserHistory::DisplayHelp()
{
  erase();
  move (0, 0);
  attroff (A_REVERSE);
  con << "One User's History help\n\n"
         "v.        view line\n"
         "u.        zero and switch to new user\n"
         "Z.        zero (wipe) history\n"
         "r.        read howto\n"
         "q.        quit this mode\n"
         "ctrl-x:   quit squidview\n"
         "\nPress any key or r: ";
  if (getch() == 'r')
    ViewHowTo();
}


cUsersMode::cUsersMode()
{
  iSortMode = nSortN;
  iLastAdjustTime = iNextAdjustTime = 0;
  iHistoryPeriod = 3600;
  fFadeFactor = 0.75;
  fEntryFactor = 1.0;
  iScreenMode = nPoints;
  iScreenStart = iSelectedRow = 0;
  iCurrentLogPos = 0;
  bUsersMonitorMode = true;
  bAcceptDenies = true;
}


bool cUsersMode::Enter()
{
  string sTemp;
  int iKey;
    
  MyCls();
  if (iCurrentLogPos == 0)
  {
    CalcPercentage (iLinePos, iLastLinePos, sTemp);
    NoLeadingSpaces (sTemp);
    con << "All Users' Tally Mode\n\nCurrent position is "
        << FilePosToTime (iRepStart)
        << " (" << sTemp << ")\n\nPress enter to begin from here.\n\n"
        << "Note this might take a while.";
    iKey = getch();
    if ((iKey == cEOL) || (iKey == cCR))
      AddData (iLinePos, iLastLinePos + 1);
    else
      return false;
    MyCls();
  }
  return Browse();
}

  
bool cUsersMode::Browse()
{
  int iKey, iMode;
  string sLine, sTemp;

  for (;;)
  {
    CalcScreen();
     
    switch (iScreenMode)
    {
      case nPoints:
        DisplayPoints();
        break;

      case nFullNames:
        DisplayFullNames();
        break;

      case nGreatest:
        DisplayGreatestURLs();
        break;

      case nLast:
        DisplayLastURLs();
        break;
      
      default:
        break;
    }
    
    CalcPercentage (iSelectedAddress, iTotalRows - 1, sLine);
    sLine += string (" ") + SortMode();
    Columnize (sLine, COLS - 1);
    sTemp = bAcceptDenies == false ? "-d " : "";
    sTemp += bUsersMonitorMode ? "mon ": "";
    sTemp += "| h = help";
    ShowMsg (sLine, sTemp);
    ShowHighLightMessage (sLine);

    if (bUsersMonitorMode)
      iKey = WaitKeyOrLog (iUsersFileSize);
    else
      while ((iKey = getch()) == 0) {}
    
    CalcScreen();
    rUserHistory& rUH = vHistory [iSelectedAddress];
     
    switch (iKey)
    {
      case 0:
        AddData (iCurrentLogPos, iUsersFileSize);
        break;
         
      case KEY_UP:
      case '8':
        if (iSelectedRow > 0)
          iSelectedRow--;
        else
          if (iScreenStart > 0)
            iScreenStart--;
        break;

      case KEY_DOWN:
      case '2':
        if (iSelectedRow < iScreenHeight - 1)
        {
          if (iSelectedAddress < iTotalRows - 1)
            iSelectedRow++;
        }
        else
          if (iScreenStart < iLastScreenRow)
            iScreenStart++;
        break;

      case KEY_PPAGE:
      case '9':
        if (iScreenStart == 0)
          iSelectedRow = 0;
        else
        {
          iScreenStart = iScreenStart - iScreenHeight;
          if (iScreenStart < 0)
            iScreenStart = 0;
        }
        break;

      case KEY_NPAGE:
      case '3':
        if (iScreenStart >= iLastScreenRow)
        {
          iScreenStart = iLastScreenRow;
          iSelectedRow = iScreenHeight - 1;
        }
        else
        {
          iScreenStart += iScreenHeight;
          if (iScreenStart > iLastScreenRow)
            iScreenStart = iLastScreenRow;
        }
        break;
      
      case KEY_HOME:
      case '7':
        iScreenStart = iSelectedRow = 0;
        break;
      
      case KEY_END:
      case '1':
        iScreenStart = iLastScreenRow;
        iSelectedRow = iScreenHeight - 1;
        break;
      
      case 'v':
        iMode = int (iScreenMode);
        if (++iMode >= int (nSMend))
          iMode = 0;
        iScreenMode = eSM (iMode);
        break;
        
      case 's':
        SortMenu();
        break;

      case 'O':
        sTemp = vHistory [iSelectedAddress].sUser;
        if (EnterUserHistory (sTemp))
          return true;
        break;

      case 'h':
        DisplayHelp();
        break;
        
      case 'r':
        ViewHowTo();
        break;
        
      case 'm':
        bUsersMonitorMode = !bUsersMonitorMode;
        break;

      case 'd':
        bAcceptDenies = !bAcceptDenies;
        break;

      case 'z':
        rUH.iPoints = rUH.iBytes = rUH.iRequests = rUH.iURLsize = 0;
        // leave rUH.iLastTime alone
        rUH.iGreatestURL = rUH.iLastURL = -1;
        break;
        
      case 'Z':
        ZeroAll();
        break;
        
      case 'q':
        return false;
        
      case 24:   // ctrl-x
        return true;
    }
  }
}


void cUsersMode::DisplayPoints()
{
  string sTemp;
  int iIndex, iLine, iCount;
  
  erase();
  move (0, 0);
  attron (A_REVERSE);
  sTemp = "User";
  Columnize (sTemp, iUserNameLen);
  con << sTemp << " ";

  DisplayHeading ("Points");
  DisplayHeading ("Bytes");
  DisplayHeading ("Requests");
  DisplayHeading ("URL size");
  DisplayHeading ("Idle");
  
  iLine = 1;
  iIndex = iScreenStart;
  iCount = 0;
  
  for (;;)
  {
    if ((iIndex >= iTotalRows) || (iCount >= iScreenHeight))
      break;
    move (iLine, 0);
    if (iCount == iSelectedRow)
      attron (A_REVERSE);
    else
      attroff (A_REVERSE);
    rUserHistory& rEntry = vHistory [iIndex];
    con << LookupUser (rEntry) << " ";
    DisplayData (rEntry.iPoints);
    DisplayData (rEntry.iBytes);
    DisplayData (rEntry.iRequests);
    DisplayData (rEntry.iURLsize);
    DisplayHeading (CalcIdle (rEntry.iLastTime));
    iIndex++;
    iLine++;
    iCount++;
  }
}


void cUsersMode::DisplayFullNames()
{
  string sLine;
  int iIndex, iLine, iCount, iNameLength;
  
  erase();
  move (0, 0);
  attron (A_REVERSE);
  con << "Full Names";
     
  iIndex = iScreenStart;
  iLine = 1;
  iCount = 0;
  iNameLength = iUserNameLen;
  if (iNameLength < 15)
    iNameLength = 15;
  
  for (;;)
  {
    if ((iIndex >= iTotalRows) || (iCount >= iScreenHeight))
      break;
    rUserHistory& rEntry = vHistory [iIndex];
    sLine = rEntry.sUser;
    Columnize (sLine, iNameLength);
    sLine = sLine + " " + rEntry.sFullName;
    Columnize (sLine, COLS - 1);
    move (iLine, 0);
    if (iCount == iSelectedRow)
      attron (A_REVERSE);
    else
      attroff (A_REVERSE);
    con << sLine;
    iIndex++;
    iLine++;
    iCount++;
  }
}


void cUsersMode::DisplayGreatestURLs()
{
  string sLine, sSize, sTemp;
  int iColumns, iIndex, iLine, iCount;
  
  erase();
  move (0, 0);
  attron (A_REVERSE);
  con << "Greatest Hits";

  iColumns = COLS;
  iIndex = iScreenStart;
  iLine = 1;
  iCount = 0;
  
  for (;;)
  {
    if ((iIndex >= iTotalRows) || (iCount >= iScreenHeight))
      break;
    rUserHistory& rEntry = vHistory [iIndex];
    sLine = LookupUser (rEntry);
    sSize = ItoCommas (rEntry.iURLsize);
    sTemp = "";
    Columnize (sTemp, nSizeCols);
    ShowMsg (sTemp, sSize);
    sLine = sLine + " " + sTemp;
    if (GetLine (rEntry.iGreatestURL) > rEntry.iGreatestURL)
    {
      GetColumn (pcReqBuff, iTargetCol, sTemp);
      RemoveSlashes (sTemp);
      sLine += " " + sTemp;
    }
    KeepExtension (sLine, iColumns - 1);
    Columnize (sLine, iColumns - 1);
    move (iLine, 0);
    if (iCount == iSelectedRow)
      attron (A_REVERSE);
    else
      attroff (A_REVERSE);
    con << sLine;
    iIndex++;
    iLine++;
    iCount++;
  }
}


void cUsersMode::DisplayLastURLs()
{
  string sLine, sSize, sTemp;
  int iColumns, iIndex, iLine, iCount;
  
  erase();
  move (0, 0);
  attron (A_REVERSE);
  con << "Last visited sites";

  iColumns = COLS;
  iIndex = iScreenStart;
  iLine = 1;
  iCount = 0;
  
  for (;;)
  {
    if ((iIndex >= iTotalRows) || (iCount >= iScreenHeight))
      break;
    rUserHistory& rEntry = vHistory [iIndex];
    sLine = LookupUser (rEntry);
    if (GetLine (rEntry.iLastURL) > rEntry.iLastURL)
    {
      GetColumn (pcReqBuff, iTargetCol, sTemp);
      RemoveSlashes (sTemp);
      sLine += " " + sTemp;
    }
    KeepExtension (sLine, iColumns - 1);
    Columnize (sLine, iColumns - 1);
    move (iLine, 0);
    if (iCount == iSelectedRow)
      attron (A_REVERSE);
    else
      attroff (A_REVERSE);
    con << sLine;
    iIndex++;
    iLine++;
    iCount++;
  }
}


void cUsersMode::DisplayHeading (const string& sHeading)
{
  string sTemp = "";
  
  Columnize (sTemp, nSizeCols);
  ShowMsg (sTemp, sHeading);
  con << sTemp << " ";
} 


void cUsersMode::DisplayData (int iData)
{
  string sTemp, sData;
  
  sTemp = "";
  sData = ItoCommas (iData);
  Columnize (sTemp, nSizeCols);
  ShowMsg (sTemp, sData);
  con << sTemp << " ";
} 


void cUsersMode::DisplayHelp()
{
  erase();
  move (0, 0);
  attroff (A_REVERSE);
  con << "All Users' Tally help\n\n"
         "v.        toggle {requests,user name,greatest hits,last site}"
         " view mode\n"
         "m.        toggle monitor mode\n"
         "d.        toggle accept denies\n"
         "s.        sort menu\n"
         "O.        one user mode\n"
         "z.        zero this user\'s stats\n"
         "Z.        zero (wipe) tally\n"
         "r.        read howto\n"
         "q.        quit users mode\n"
         "ctrl-x:   quit squidview\n"
         "\nPress any key or r: ";
  if (getch() == 'r')
    ViewHowTo();
}


void cUsersMode::CalcScreen()
{
  tFilePos iCurrent;
  
  iCurrent = GetFileSize (pszCurrentLog);
  if (iCurrent < iUsersFileSize)
    ZeroAll();
  iUsersFileSize = iCurrent;
  iSelectedAddress = iScreenStart + iSelectedRow;
  iScreenHeight = LINES - 2;
  if (iScreenHeight > iTotalRows)
    iScreenHeight = iTotalRows;
  iLastScreenRow = iTotalRows - iScreenHeight;
  if (iLastScreenRow < 0)
    iLastScreenRow = 0;
  FindFullNames();
}


string cUsersMode::LookupUser (const rUserHistory& aUser)
{
  string sTemp;
  
  sTemp = aUser.sUser;
  if (int (sTemp.length()) > iUserNameLen)
    sTemp = ChopOffNetwork (sTemp, iUserNameLen);
  Columnize (sTemp, iUserNameLen);
  return sTemp;
}


void cUsersMode::AddData (tFilePos iFrom, tFilePos iTo)
{
  tFilePos iCurrent, iTempPos;
  int iCount, iRequestSize, iThisEntry, iTemp;
  time_t iTime;
  string sUserName;
  rUserHistory aEntry;
  char cRequestFlag;
  eSort iOriginalSort;
  
  iCurrent = iFrom;
  iOriginalSort = iSortMode;
  if (iOriginalSort == nSortN)
    iOriginalSort = nSortP;
  iSortMode = nSortR;

  CheckUpdate();
  GoForNoDelay();
  
  for (;;)
  {
    if (iCurrent >= iTo)
      break;
    iTempPos = GetLine (iCurrent);
    if ((iTempPos == 0) || (iTempPos == iCurrent))
      break;

    if (!NullText (pcReqBuff))
    {
      iRequestSize = GetRequestSize (pcReqBuff);
      sUserName = GetUserName (pcReqBuff, false);

      if (bAcceptDenies)
        cRequestFlag = ' ';
      else
        GetRequestCacheHit (pcReqBuff, cRequestFlag, iTemp);

      if ((sUserName != "") && (cRequestFlag != 'd'))
      {
        iThisEntry = -1;
        for (iCount = vHistory.size() - 1; iCount >= 0; iCount--)
        {
          if (sUserName == vHistory [iCount].sUser)
          {
            iThisEntry = iCount;
            break;
          }
        }
        if (iThisEntry == -1)
        {
          aEntry.Zero();
          aEntry.sUser = sUserName;
          vHistory.push_back (aEntry);
          iThisEntry = vHistory.size() - 1;
        }
        
        iTime = GetTimeNumber (pcReqBuff);
        if (iTime >= iNextAdjustTime)
        {
          if (iNextAdjustTime == 0)
          {
            iLastAdjustTime = CalcAeon (iTime);
            iNextAdjustTime = iLastAdjustTime + iHistoryPeriod;
          }
          else
            AgeData (iTime);
        }
        
        rUserHistory& aIndex = vHistory [iThisEntry];
        aIndex.iBytes += iRequestSize;
        aIndex.iRequests++;
        if (iRequestSize > aIndex.iURLsize)
        {
          aIndex.iURLsize = iRequestSize;
          aIndex.iGreatestURL = iCurrent;
        }
        aIndex.iPoints += int (iRequestSize * fEntryFactor);
        aIndex.iLastTime = iTime;
        aIndex.iLastURL = iCurrent;
      }
    }

    iCurrentLogPos = iTempPos;
    
    if (CheckInterrupt())
    {
      bUsersMonitorMode = false;
      break;
    }
    if (CheckTicker())
    {
      SortData();
      UpdateTicker ("Users mode gathering", iCurrent - iFrom, iTo - iFrom);
    }
    iCurrent = iTempPos;
  }

  if (wTemp)
    nodelay (wTemp, false);

  iTotalRows = vHistory.size();
  iSortMode = iOriginalSort;
  SortData();
}


void cUsersMode::SortData()
{
  bool bDone, bSwitch;
  int iIndex;
  rUserHistory rUH;

  iSortFlag = 312;
  
  for (;;)
  {
    bDone = true;
    for (iIndex = vHistory.size() - 2; iIndex >= 0; iIndex--)
    {
      bSwitch = false;
      rUserHistory& rUHleft = vHistory [iIndex];
      rUserHistory& rUHright = vHistory [iIndex + 1];
      switch (iSortMode)
      {
        case nSortU:
          if (rUHleft.sUser > rUHright.sUser)
            bSwitch = true;
          break;

        case nSortP:
          if (rUHleft.iPoints < rUHright.iPoints)
            bSwitch = true;
          break;

        case nSortB:
          if (rUHleft.iBytes < rUHright.iBytes)
            bSwitch = true;
          break;
          
        case nSortR:
          if (rUHleft.iRequests < rUHright.iRequests)
            bSwitch = true;
          break;

        case nSortURL:
          if (rUHleft.iURLsize < rUHright.iURLsize)
            bSwitch = true;
          break;

        case nSortI:
          if (rUHleft.iLastTime < rUHright.iLastTime)
            bSwitch = true;
          break;
          
        case nSortN:
          break;
      }
      if (bSwitch)
      {
        bDone = false;
        rUH = rUHleft;
        rUHleft = rUHright;
        rUHright = rUH;
      }
    }
    if (bDone)
    {
      iSortFlag = 0;
      return;
    }
  }
}


const char* cUsersMode::SortMode()
{
  switch (iSortMode)
  {
    case nSortU:
      return "user name";
      
    case nSortP:
      return "points";
      
    case nSortB:
      return "bytes";
      
    case nSortR:
      return "requests";
      
    case nSortURL:
      return "URL size";

    case nSortI:
      return "idle";

    case nSortN:
    default:
      return "unsorted";      
  }
}


void cUsersMode::FindFullNames()
{
  int iFile, iIndex, iRead;
  string sLine, sTemp;
  char cLetter;
  
  static int iLastTotal = -1;
  
  if (iTotalRows == iLastTotal)
    return;
  iLastTotal = iTotalRows;
  
  iFile = open (sUsersFile.c_str(), O_RDONLY);
  if (iFile > 0)
  {
    sLine = "";
    for (;;)
    {
      iRead = read (iFile, &cLetter, 1);
      if (iRead < 1)
        break;
      if ((cLetter == cEOL) || (cLetter == cCR))
      {
        if (sLine != "")
        {
          GetColumn (sLine.c_str(), 1, sTemp);
          for (iIndex = 0; iIndex < iTotalRows; iIndex++)
          {
            rUserHistory& rEntry = vHistory [iIndex];
            if (sTemp == rEntry.sUser)
            {
              sLine.replace (0, sTemp.length() + 1, "");
              rEntry.sFullName = sLine;
              break;
            }
          }
          sLine = "";
        }
      }
      else
        sLine += cLetter;
    }
    close (iFile);
  }
}


void cUsersMode::SortMenu()
{
  int iKey;
  
  erase();
  move (0, 0);
  attroff (A_REVERSE);
  con << "Current sort method is: " << SortMode()
      << "\n\nChange to:\n\n"
         "1.  user name\n"
         "2.  points\n"
         "3.  bytes\n"
         "4.  requests\n"
         "5.  URL size\n"
         "6.  idle time\n"
         "\nAny other key to dismiss";
  iKey = getch();
  if ((iKey >= '1') && (iKey <= '6'))
  {
    con << "\n\nSorting...";
    refresh();
    iSortMode = cUsersMode::eSort ((iKey - '1') + nSortU);
    SortData();
    iScreenStart = iSelectedRow = 0;
  }
}


void cUsersMode::AgeData (int iCurrentTime)
{
  int iAge, iIndex, iTotal;
  float fFactor;
  
  iAge = (CalcAeon (iCurrentTime) - iLastAdjustTime) / iHistoryPeriod;
  fFactor = pow (fFadeFactor, iAge);
  iTotal = vHistory.size();
  for (iIndex = 0; iIndex < iTotal; iIndex++)
    vHistory [iIndex].iPoints = int (vHistory [iIndex].iPoints * fFactor);
  iLastAdjustTime = CalcAeon (iCurrentTime);
  iNextAdjustTime = iLastAdjustTime + iHistoryPeriod;
}


void cUsersMode::ZeroAll()
{
  while (!vHistory.empty())
    vHistory.pop_back();
  iTotalRows = 0;
  iScreenStart = iSelectedRow = 0;
  iCurrentLogPos = 0;
}


// list of keystrokes
void Help()
{
  MyCls();
  con << PACKAGE << " " << VERSION << ": (c) " << "2001-2017"
      << " Graeme Sheppard - GPL software\n"
      << "www.rillion.net/squidview\n\n" <<
    "Keystrokes:-\n\n"
    "Up/Down/PgUp/PgDn:   browse\n"
    "Home/End/g/j/J:      goto certain location in log\n"
    "Left/Right:          search up/down\n"
    "1-9:                 numlock and keypad strokes if necessary\n"
    "f/F:                 find text\n"
    "o:                   find options\n"
    "v:                   verbose listing of selected line\n"
    "p/s/t:               primary / secondary / tertiary log file\n"
    "m:                   toggle monitor mode\n"
    "l:                   log a report\n"
    "n:                   misc. log functions\n"
    "c:                   common options\n"
    "T:                   all users' tally mode\n"
    "O:                   one user history mode\n"
    "r:                   read the squidview howto with " << sViewer << "\n"
    "ctrl-x:              quit\n"
    "\nPress q to quit this screen, r to read the howto.";
  refresh();

  for (;;)
  {
    switch (getch())
    {
      case 'r':
        ViewHowTo();
      case 'q':
        return;
    }
  }
}


// the routine to draw the screen
void DrawMain()
{
  string sLine, sFlags, sTarget, sSelectedTime, sTemp;
  int iCount;
  tFilePos iThis, iNext;
  time_t iTime;

  iLinePos = 0;
  iThis = iPagePos;
  sSelectedTime = "";

  for (iCount = 0; iCount < iMainLines; iCount++)
  {
    move (iCount, 0);
    iNext = GetLine (iThis);
    if (iNext > iThis)
    {
      GetPaddedUserName (pcReqBuff, sLine);
      GetRequestFlags (pcReqBuff, sFlags);
      GetColumn (pcReqBuff, iTargetCol, sTarget);
      RemoveSlashes (sTarget);
      sLine += " " + sFlags + " " + sTarget;
      KeepExtension (sLine, COLS - 1);
      Columnize (sLine, COLS - 1);
      if (iCount == iLinesDown)
      {
        attron (A_REVERSE);
        iLinePos = iThis;
        iTime = GetTimeNumber (pcReqBuff);
        CalcTime (iTime, sSelectedTime);
        RemoveSeconds (sSelectedTime);
      }
      else
        attroff (A_REVERSE);
      con << sLine;
      iThis = iNext;
    }
    else
    {
      sLine = "";
      Columnize (sLine, COLS - 1);
      attroff (A_REVERSE);
      con << sLine;
    }
  }

  CalcPercentage (iLinePos, iLastLinePos, sLine);
  sLine += " " + sSelectedTime;
  Columnize (sLine, COLS - 1);
  if (iMonitorMode)
    sTemp = "Mon ";
  else
    sTemp = " ";
  sTemp += string (pszCurrentName) + " | h = help";
  ShowMsg (sLine, sTemp);

  if (sStatusMessage != "")
  {
    ShowHighLightMessage (sStatusMessage);
    sStatusMessage = "";
  }
  else
    ShowHighLightMessage (sLine);

  refresh();
}


// finish up
static void FinishSig (int iSig)
{
  endwin();
  if (fReadingFile > 0)
    close (fReadingFile);  
  exit (iSig);
}


// darn STL?
static void SegFaultSig (int iSig)
{
  endwin();
  RunProgramInConsole ("reset -Q ; clear");
  if (iSortFlag >= 1000)
    printf ("Squidview: fault during STL sort()\n"
            "code = %d\n"
            "Try:\n"
            " 1. increasing iRepFast or making it 0\n"
            " 2. using my sort routine\n"
            "Refer to the BUGS file.\n"
            "Sorry.\n",
            iSortFlag);
  else
    printf ("Squidview: general fault\n"
            "code = %d\n"
            "Refer to the BUGS file.\n"
            "Sorry.\n",
            iSortFlag);
  exit (iSig);
}


bool FileExists (const string &file)
{
  return (access (file.c_str(), F_OK) == 0);
}


bool FileReadable (const string &file)
{
  return (access (file.c_str(), R_OK) == 0);
}


// is this the first run?
// if so create .squidview directory and make links
void CreateLinks()
{
  char* szTemp;
  int iTemp;
  string sLog1, sLog2, sLog3, sHowToFile, sLogLocation;

  szTemp = getenv ("HOME");
  if (szTemp == 0)
  {
    print ("squidview: can't get your home directory, exiting.");
    exit (1);
  }

  sHomeDir = szTemp;
  sPathToFiles = sHomeDir + "/" + szSquidHome;

  sLogFile1 = sPathToFiles + "/" + szLog1;
  sLogFile2 = sPathToFiles + "/" + szLog2;
  sLogFile3 = sPathToFiles + "/" + szLog3;
  sWordFile = sPathToFiles + "/" + szWordFile;
  sUsersFile = sPathToFiles + "/" + szUsersFile;
  sAliasesFile = sPathToFiles + "/" + szAliasesFile;
  sSetFileName = sPathToFiles + "/squidview.conf";
  sHowToLink = sPathToFiles + "/" + szHowToFile;
  sHowToFile = string (SHAREDIR) + "/" + szHowToFile;

  if (FileExists (sPathToFiles))
  {
    if (!FileReadable (sLogFile1))
    {
      print ("The squid log file cannot be read.");
      exit (1);
    }
    return; // all is well hopefully
  }

  print ("Making .squidview directory and links...");

// bug fix contributed by Willi Mann: check for file existance, not size
  sLogLocation = "";
  bool LogFileFound = false;
  for (iTemp = 0; sLogLocations [iTemp] != ""; iTemp++)
    if (FileExists (sLogLocations [iTemp]))
    {
      sLogLocation = sLogLocations [iTemp];
      LogFileFound = true;
      break;
    }

  if(!LogFileFound)
    sLogLocation = sLogLocations[0];

  iTemp = 0;
  for (;;)
  {
    if (mkdir (sPathToFiles.c_str(), 0755)) break;
    if (symlink (sLogLocation.c_str(), sLogFile1.c_str())) break;
    if (symlink (szLog1, sLogFile2.c_str())) break;
    if (symlink (szLog1, sLogFile3.c_str())) break;
    if (symlink (sHowToFile.c_str(), sHowToLink.c_str())) break;
    iTemp = 1;
    break;
  }
  if (iTemp == 0)
  {
    print ("~/.squidview/ setup errors. Exiting.");
    exit (1);
  }

  if (!FileReadable (sLogFile1))
  {
    print ("The squid log file cannot be accessed at pre-programmed locations.");
    perror("The error was");
    print ("You may need to manually adjust the symlinks in ~/.squidview/");
    print ("You may need to give user permissions to log files, or add to a group (logout.)");

#ifdef DEBIANIZED
    print ("You may want to add your username to the group proxy.\n");
#endif

    print ("Or, after admin adjustments delete ~/.squidview/ and re-run squidview.");
    exit (1);
  }
}


// main is also the main loop
int main (int, char*[])
{
  int iKey, iCount;
  SettingsIO cSettings;
  GenerateReport cReport;
  string sTemp;
  bool bExit;

  CreateLinks();    // better put this here

  signal (SIGINT, FinishSig);
  signal (SIGSEGV, SegFaultSig);
  initscr();
  keypad (stdscr, TRUE);
  nonl();
  cbreak();
  noecho();
  curs_set (0);

  iLogFileSize = 0;
  cSettings.Load();
  ChangeLog (iCurrentLog);
  sStatusMessage = "";
  LoadWords();
  LoadAliases();
  
  CheckUpdate();

  for (;;)
  {
    if (wTemp)
      nodelay (wTemp, false);
    CheckUpdate();
    DrawMain();

    if (iMonitorMode == 0)
    {
      while ((iKey = getch()) == 0) {}
    }
    else
    {
      iKey = WaitKeyOrLog (iLogFileSize);
      if (iKey == 0)
        iKey = KEY_END;
    }

    CheckUpdate();
    bExit = false;

    switch (iKey)
    {
      case KEY_UP:
      case '8':
        if (iLinesDown > 0)
          iLinesDown--;
        else
          iPagePos = GetPrevLine (iPagePos);
        break;

      case KEY_DOWN:
      case '2':
        if (iLinesDown < iMaxLinesDown)
          iLinesDown++;
        else
        {
          if (iPagePos < iLastPage)
            iPagePos = GetLine (iPagePos);
        }
        break;

      case KEY_HOME:
      case '7':
        iPagePos = 0;
        iLinesDown = 0;
        break;

      case KEY_END:
      case '1':
        iPagePos = iLastPage;
        iLinesDown = iMaxLinesDown;
        break;

      case KEY_PPAGE:
      case '9':
        for (iCount = 0; iCount < iMainLines; iCount++)
          iPagePos = GetPrevLine (iPagePos);
        if (iPagePos == 0)
          iLinesDown = 0;
        break;

      case KEY_NPAGE:
      case '3':
        for (iCount = 0; iCount < iMainLines; iCount++)
          iPagePos = GetLine (iPagePos);
        if (iPagePos >= iLastPage)
        {
          iPagePos = iLastPage;
          iLinesDown = iMaxLinesDown;
        }
        break;

      case KEY_LEFT:
      case '4':
        GoForNoDelay();
        SearchWords (-1);
        break;

      case KEY_RIGHT:
      case '6':
        GoForNoDelay();
        SearchWords (1);
        break;

      case 'g':
        GotoPercentage();
        break;

      case 'j':
        JumpToDayForward();
        break;

      case 'J':
        JumpToDayBackward();
        break;

      case 'f':
        PrependFindText();
        break;

      case 'F':
        AppendFindText();
        break;

      case 'o':
        SearchOptions();
        break;

      case 'c':
        CommonOptions();
        break;

      case 'n':
        MiscLogOptions();
        break;

      case 'm':
        if (iMonitorMode == 0)
          iMonitorMode = 1;
        else
          iMonitorMode = 0;
        break;

      case 'l':
        if (ReportOptions())
          cReport.Run();
        break;

      case 'p':
        ChangeLog (1);
        break;

      case 's':
        ChangeLog (2);
        break;

      case 't':
        ChangeLog (3);
        break;

      case 'v':
        ViewSelectedLine (iLinePos);
        break;

      case 'h':
        Help();
        break;

      case 'r':
        ViewHowTo();
        break;

      case ' ':
        MyCls();
        DrawMain();
        break;

      case 'd':
        MyCls();
        con << sDebugText;
        getch();
        break;

      case 'O':
        GetLine (iLinePos);
        bExit = EnterUserHistory (GetUserName (pcReqBuff, false));
        break;

      case 'T':
        bExit = aTally.Enter();
        break;

      case 24:   // ctrl-x
        bExit = true;
        break;
    }

    if (bExit)
    {
      cSettings.Save();
      FinishSig (0);
    }
  }
}
